package ui;

import java.awt.Color;
import java.awt.Desktop;
import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import kruskalsolution.Edge;
import kruskalsolution.KruskalMST;
import kruskalsolution.WeightedQuickUnion;

public class MainGUI {

	private JFrame frame;
	private JButton btnBrowseSource;
	private JLabel lblSource;
	private JButton btnBrowseResult;
	private JTextArea textArea;
	private JLabel lblStatus;
	private JButton btnShowResult;
	private JLabel lblResult;
	private JButton btnOneStepExecution;
	private JComboBox comboBox1;
	private JComboBox comboBox2;
	private JButton btnExecute;
	private JLabel lblOneTimeExecution;
	private JLabel lblControlledExecution;
	private boolean firstStep = true;
	private KruskalMST kRunner;
	private List<Edge> summationMTS = new ArrayList<Edge>();
	private JComboBox comboBoxMoreSteps;
	private JButton btnRefresh;
	private JButton btnHelp;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainGUI window = new MainGUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainGUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBackground(SystemColor.text);
		frame.setResizable(false);
		frame.setBounds(100, 100, 847, 700);
		frame.setTitle("Minimum Spanning Tree GUI");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 20, 361, 82, 88, 130, 97, 0, 0 };
		gridBagLayout.rowHeights = new int[] { 24, 49, 38, 47, 57, 0, 44, 347,
				25, 0 };
		gridBagLayout.columnWeights = new double[] { 0.0, 0.0, 1.0, 0.0, 1.0,
				0.0, 2.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
				0.0, 0.0, 0.0, Double.MIN_VALUE };
		frame.getContentPane().setLayout(gridBagLayout);

		this.btnBrowseSource = new JButton("   Browse   ");
		btnBrowseSource.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser saveFile = new JFileChooser();
				if (saveFile.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
					String fileName = saveFile.getSelectedFile()
							.getAbsolutePath();
					lblSource.setText(fileName);
					btnOneStepExecution.setEnabled(true);
					comboBoxMoreSteps.setEnabled(true);
					firstStep = true;
				}
			}
		});

		btnHelp = new JButton("Help");
		btnHelp.setToolTipText("open pdf format of help");
		btnHelp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					Desktop.getDesktop().open(new File("./Help.pdf"));
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		GridBagConstraints gbc_btnHelp = new GridBagConstraints();
		gbc_btnHelp.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnHelp.insets = new Insets(0, 0, 5, 5);
		gbc_btnHelp.gridx = 5;
		gbc_btnHelp.gridy = 0;
		frame.getContentPane().add(btnHelp, gbc_btnHelp);

		JLabel lblNewLabel = new JLabel(
				"Please choose your graph information file");
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel.gridx = 1;
		gbc_lblNewLabel.gridy = 1;
		frame.getContentPane().add(lblNewLabel, gbc_lblNewLabel);
		GridBagConstraints gbc_btnBrowseSource = new GridBagConstraints();
		gbc_btnBrowseSource.anchor = GridBagConstraints.EAST;
		gbc_btnBrowseSource.insets = new Insets(0, 0, 5, 5);
		gbc_btnBrowseSource.gridx = 5;
		gbc_btnBrowseSource.gridy = 1;
		frame.getContentPane().add(btnBrowseSource, gbc_btnBrowseSource);

		this.lblSource = new JLabel("");
		lblSource.setForeground(Color.BLUE);
		GridBagConstraints gbc_lblSource = new GridBagConstraints();
		gbc_lblSource.anchor = GridBagConstraints.WEST;
		gbc_lblSource.insets = new Insets(0, 0, 5, 5);
		gbc_lblSource.gridx = 1;
		gbc_lblSource.gridy = 2;
		frame.getContentPane().add(this.lblSource, gbc_lblSource);

		this.btnBrowseResult = new JButton("   Browse   ");
		this.btnBrowseResult.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser saveFile = new JFileChooser();
				if (saveFile.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
					String fileName = saveFile.getSelectedFile()
							.getAbsolutePath();
					lblResult.setText(fileName);
				}
			}
		});

		JLabel lblNewLabel_1 = new JLabel(
				"Please choose a plcae where you want to save your result");
		GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
		gbc_lblNewLabel_1.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_1.gridx = 1;
		gbc_lblNewLabel_1.gridy = 3;
		frame.getContentPane().add(lblNewLabel_1, gbc_lblNewLabel_1);
		GridBagConstraints gbc_btnBrowseResult = new GridBagConstraints();
		gbc_btnBrowseResult.anchor = GridBagConstraints.EAST;
		gbc_btnBrowseResult.insets = new Insets(0, 0, 5, 5);
		gbc_btnBrowseResult.gridx = 5;
		gbc_btnBrowseResult.gridy = 3;
		frame.getContentPane().add(this.btnBrowseResult, gbc_btnBrowseResult);

		this.lblResult = new JLabel("");
		lblResult.setForeground(Color.BLUE);
		GridBagConstraints gbc_lblResult = new GridBagConstraints();
		gbc_lblResult.anchor = GridBagConstraints.WEST;
		gbc_lblResult.insets = new Insets(0, 0, 5, 5);
		gbc_lblResult.gridx = 1;
		gbc_lblResult.gridy = 4;
		frame.getContentPane().add(this.lblResult, gbc_lblResult);

		this.comboBox1 = new JComboBox();

		lblOneTimeExecution = new JLabel("One Time Execution");
		GridBagConstraints gbc_lblOneTimeExecution = new GridBagConstraints();
		gbc_lblOneTimeExecution.insets = new Insets(0, 0, 5, 5);
		gbc_lblOneTimeExecution.anchor = GridBagConstraints.WEST;
		gbc_lblOneTimeExecution.gridx = 1;
		gbc_lblOneTimeExecution.gridy = 5;
		frame.getContentPane()
				.add(lblOneTimeExecution, gbc_lblOneTimeExecution);

		this.comboBox2 = new JComboBox();
		comboBox2.setModel(new DefaultComboBoxModel(new String[] { "Kruskal",
				"Prim" }));

		GridBagConstraints gbc_comboBox_1 = new GridBagConstraints();
		gbc_comboBox_1.insets = new Insets(0, 0, 5, 5);
		gbc_comboBox_1.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBox_1.gridx = 2;
		gbc_comboBox_1.gridy = 5;
		frame.getContentPane().add(comboBox2, gbc_comboBox_1);

		this.btnShowResult = new JButton("Show Result");
		btnShowResult.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(lblResult.getText().equals("")){
					JOptionPane.showMessageDialog(null,
							"Please enter result file path!");
				}else{
					try (BufferedReader br = new BufferedReader(new FileReader(
							lblResult.getText()))) {
						String line;
						while ((line = br.readLine()) != null) {
							textArea.append(line + "\n");
						}
					} catch (Exception e) {
						JOptionPane.showMessageDialog(null,
								e.getMessage());
					}
				}
			}
		});

		this.btnExecute = new JButton("    Execute   ");
		this.btnExecute.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (lblResult.getText().equals("")
						|| lblSource.getText().equals("")) {
					JOptionPane.showMessageDialog(null,
							"Please enter source file and result file path!");
				} else {
					if (comboBox2.getSelectedIndex() == 0) {
						Thread t = new Thread(new Runnable() {
							@Override
							public void run() {
								btnShowResult.setVisible(true);
								lblStatus
										.setText("Current Status: In Progress!");
								KruskalMST k = new KruskalMST(lblSource
										.getText(), lblResult.getText());
								if (k.finder()) {
									textArea.setText("");
									lblStatus.setText("Current status: Done!");
								}
							}
						});
						t.start();
					} else {
						btnShowResult.setVisible(false);
						textArea.setText("Prime algorithm has not implemented yet!");
						lblStatus.setText("Current Status: Not Started!");
					}
				}
			}
		});
		GridBagConstraints gbc_btnExecute = new GridBagConstraints();
		gbc_btnExecute.insets = new Insets(0, 0, 5, 5);
		gbc_btnExecute.gridx = 3;
		gbc_btnExecute.gridy = 5;
		frame.getContentPane().add(btnExecute, gbc_btnExecute);
		GridBagConstraints gbc_btnShowResult = new GridBagConstraints();
		gbc_btnShowResult.insets = new Insets(0, 0, 5, 5);
		gbc_btnShowResult.gridx = 4;
		gbc_btnShowResult.gridy = 5;
		frame.getContentPane().add(this.btnShowResult, gbc_btnShowResult);

		lblControlledExecution = new JLabel("Controlled Execution");
		GridBagConstraints gbc_lblControlledExecution = new GridBagConstraints();
		gbc_lblControlledExecution.insets = new Insets(0, 0, 5, 5);
		gbc_lblControlledExecution.anchor = GridBagConstraints.WEST;
		gbc_lblControlledExecution.gridx = 1;
		gbc_lblControlledExecution.gridy = 6;
		frame.getContentPane().add(lblControlledExecution,
				gbc_lblControlledExecution);
		comboBox1.setModel(new DefaultComboBoxModel(new String[] { "Kruskal",
				"Prim" }));
		GridBagConstraints gbc_comboBox_11 = new GridBagConstraints();
		gbc_comboBox_11.insets = new Insets(0, 0, 5, 5);
		gbc_comboBox_11.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBox_11.gridx = 2;
		gbc_comboBox_11.gridy = 6;
		frame.getContentPane().add(comboBox1, gbc_comboBox_11);

		comboBoxMoreSteps = new JComboBox();

		comboBoxMoreSteps.setModel(new DefaultComboBoxModel(new String[] {
				"1 Step", "5 Steps", "10 Steps", "20 Steps", "100 Steps" }));
		GridBagConstraints gbc_comboBoxMoreSteps = new GridBagConstraints();
		gbc_comboBoxMoreSteps.insets = new Insets(0, 0, 5, 5);
		gbc_comboBoxMoreSteps.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBoxMoreSteps.gridx = 3;
		gbc_comboBoxMoreSteps.gridy = 6;
		frame.getContentPane().add(comboBoxMoreSteps, gbc_comboBoxMoreSteps);

		this.btnOneStepExecution = new JButton("    Execute   ");
		btnOneStepExecution.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Thread t = new Thread(new Runnable() {
					@Override
					public void run() {
						if (lblResult.getText().equals("")
								|| lblSource.getText().equals("")) {
							JOptionPane
									.showMessageDialog(null,
											"Please enter source file and result file path!");
						} else {
							if (comboBox1.getSelectedIndex() == 1) {
								JOptionPane
										.showMessageDialog(null,
												"Prim Algorithm has not implemented yet!");
							} else {
								lblStatus
										.setText("Current Status: In Progress!");
								if (firstStep == true) {
									kRunner = new KruskalMST(lblSource
											.getText(), lblResult.getText());
									textArea.setText("");
									kRunner.setStepedQuickUnion(new WeightedQuickUnion(
											kRunner.getVertices()));
									textArea.append("Total number of nodes = "
											+ "" + kRunner.getVertices() + "\n");
									textArea.append("Total number of edges in the minimum spanning three = "
											+ ""
											+ (kRunner.getVertices() - 1)
											+ "\n");
									textArea.append("List of edges & their costs:\n");
									firstStep = false;
								}

								if (kRunner.getEdges().size() < 1) {
									btnOneStepExecution.setEnabled(false);
									textArea.append("Total cost of minimum spanning three is = Sum of ");
									textArea.append(kRunner
											.getMSTWeight(summationMTS));
									lblStatus.setText("Current Status: Done!");
								} else {
									int indexer = 1;
									switch (comboBoxMoreSteps
											.getSelectedIndex()) {
									case 0:
										indexer = 1;
										break;
									case 1:
										indexer = 5;
										break;
									case 2:
										indexer = 10;
										break;
									case 3:
										indexer = 20;
										break;
									case 4:
										indexer = 100;
										break;
									default:
										break;
									}
									for (int i = 0; i < indexer; i++) {
										Edge edge = kRunner.ControlledKruskal();
										if (edge != null) {
											textArea.append(edge.toString()
													+ "\n");
											summationMTS.add(edge);
										} else {
											if (kRunner.getEdges().size() >= 1) {
												Edge node = kRunner.getEdges()
														.get(0);
												textArea.append(node.toString()
														+ "  ---> cycle\n");
												kRunner.getEdges().remove(node);
											}

										}
										if (kRunner.getEdges().size() < 1) {
											textArea.append("Total cost of minimum spanning three is = Sum of ");
											textArea.append(kRunner
													.getMSTWeight(summationMTS));
											btnOneStepExecution
													.setEnabled(false);
											lblStatus
													.setText("Current Status: Done!");
											kRunner = null;
											break;
										}
									}
								}
							}
						}
					}
				});
				t.start();
			}
		});
		GridBagConstraints gbc_btnPause = new GridBagConstraints();
		gbc_btnPause.insets = new Insets(0, 0, 5, 5);
		gbc_btnPause.gridx = 4;
		gbc_btnPause.gridy = 6;
		frame.getContentPane().add(this.btnOneStepExecution, gbc_btnPause);

		btnRefresh = new JButton("   Refresh   ");
		btnRefresh.setToolTipText("refresh and try");
		btnRefresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				textArea.setText("");
				firstStep = true;
				btnOneStepExecution.setEnabled(true);
				summationMTS = new ArrayList<Edge>();
				comboBoxMoreSteps.setSelectedIndex(0);
				lblStatus.setText("Current Status: Not Started!");
			}
		});
		GridBagConstraints gbc_btnRefresh = new GridBagConstraints();
		gbc_btnRefresh.anchor = GridBagConstraints.EAST;
		gbc_btnRefresh.insets = new Insets(0, 0, 5, 5);
		gbc_btnRefresh.gridx = 5;
		gbc_btnRefresh.gridy = 6;
		frame.getContentPane().add(btnRefresh, gbc_btnRefresh);

		this.textArea = new JTextArea();
		this.textArea.setLineWrap(true);
		this.textArea.setWrapStyleWord(true);
		this.textArea.setEditable(false);
		JScrollPane scroll = new JScrollPane(this.textArea,
				JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		GridBagConstraints gbc_textArea = new GridBagConstraints();
		gbc_textArea.gridwidth = 5;
		gbc_textArea.insets = new Insets(0, 0, 5, 5);
		gbc_textArea.fill = GridBagConstraints.BOTH;
		gbc_textArea.gridx = 1;
		gbc_textArea.gridy = 7;
		frame.getContentPane().add(scroll, gbc_textArea);

		this.lblStatus = new JLabel("Current Status: Not Started!");
		lblStatus.setForeground(Color.BLACK);
		GridBagConstraints gbc_lblStatus = new GridBagConstraints();
		gbc_lblStatus.anchor = GridBagConstraints.WEST;
		gbc_lblStatus.insets = new Insets(0, 0, 0, 5);
		gbc_lblStatus.gridx = 1;
		gbc_lblStatus.gridy = 8;
		frame.getContentPane().add(this.lblStatus, gbc_lblStatus);
	}

}
