﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment2
{
    class WeightedQuickUnion
    {

        protected int[] unionArray;
        protected int[] unionSize;

        public WeightedQuickUnion(int n)
        {
            try
            {
                unionArray = new int[n];
                unionSize = new int[n];
                for (int i = 0; i < unionArray.Length; i++)
                {
                    unionArray[i] = i;
                    unionSize[i] = 1;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("QuickUnion--> initialize method" + e.Message);
            }
        }

        public bool connected(int p, int q)
        {
            return root(p) == root(q);
        }

        public int root(int p)
        {
            try
            {
                while (p != unionArray[p])
                {
                    p = unionArray[p];
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("QuickUnion--> root method" + e.Message);
            }
            return p;
        }

        public void union(int p, int q)
        {
            try
            {
                var i = root(p);
                var j = root(q);
                if (i == j) return;
                if (unionSize[i] < unionSize[j])
                {
                    unionArray[i] = j;
                    unionSize[j] += unionSize[i];
                }
                else {
                    unionArray[j] = i;
                    unionSize[i] += unionSize[j];
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("QuickUnion--> union method" + e.Message);
            }

        }

    }
}
