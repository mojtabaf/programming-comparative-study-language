﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Assignment3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("For Stopping the application press enter.....!");
            var simulator = new Simulator();
            Thread app = new Thread(new ThreadStart(simulator.run));
            app.Start();
            Console.ReadLine();
            simulator.shutdown();
            app.Join();
            Console.WriteLine("done!");
            Console.ReadLine();
        }
    }
}
