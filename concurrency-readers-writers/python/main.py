from simulator import Simulator
import threading

if __name__ == "__main__":
    print("For Stopping the application press enter.....!")
    simulator = Simulator()
    app = threading.Thread(target=simulator.run, name="App")
    app.start()
    input()
    simulator.shutdown()
    app.join()
    print("done!")



