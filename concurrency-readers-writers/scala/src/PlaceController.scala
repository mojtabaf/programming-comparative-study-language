import scala.collection.mutable
import scala.util.Random


class PlaceController {
  var reader = 0

  var sharedList = new mutable.MutableList[Int]
  val list_limit = 10

  def read(): Unit = {
    this.synchronized {
      this.reader += 1
      val name = Thread.currentThread().getName
      print("Thread " + name + " now is ready to read the shared location.\t ")
    }

    println(this.sharedList.toString())
    val delay = (Math.random() * 3000).toLong
    Thread.sleep(delay)

    this.synchronized {
      val name = Thread.currentThread().getName
      println("Thread " + name + " now has finished reading the shared location.")
      this.reader -= 1
      if (this.reader == 0) {
        this.notifyAll()
      }
    }
  }

  def write():Unit = synchronized{
    while(this.reader != 0){
      this.wait()
    }
    val name = Thread.currentThread().getName
    println("Thread " + name + " now is ready to write the shared location.\t ")
    if(this.sharedList.size > this.list_limit) {
      this.sharedList.drop(1)
    }
    val random = new Random()
    val number = random.nextInt(100)
    sharedList += number
    val delay = (Math.random() * 3000).toLong
    Thread.sleep(delay)
    println("Thread " + name + " now has finished writing the shared location. " + number)
    this.notifyAll()
  }
}
