from random import randint
from sharedplace import SharedPlaceController
import threading


class Simulator:

    def __init__(self):
        self.running = True

    def run(self):
        shared_place = SharedPlaceController()
        print("%%%%%%%%%%%%%%%%%%%% Simulate a draw of a dice %%%%%%%%%%%%%%%%%%%%\n")
        while self.running:
            if randint(0, 6) % 2 == 0:
                t1 = threading.Thread(target=shared_place.rworker, name="Reader 1")
            else:
                t1 = threading.Thread(target=shared_place.wworker, name="Writer 1")

            if randint(0, 6) % 2 == 0:
                t2 = threading.Thread(target=shared_place.rworker, name="Reader 2")
            else:
                t2 = threading.Thread(target=shared_place.wworker, name="Writer 2")

            if randint(0, 6) % 2 == 0:
                t3 = threading.Thread(target=shared_place.rworker, name="Reader 3")
            else:
                t3 = threading.Thread(target=shared_place.wworker, name="Writer 3")

            if randint(0, 6) % 2 == 0:
                t4 = threading.Thread(target=shared_place.rworker, name="Reader 4")
            else:
                t4 = threading.Thread(target=shared_place.wworker, name="Writer 4")

            if randint(0, 6) % 2 == 0:
                t5 = threading.Thread(target=shared_place.rworker, name="Reader 5")
            else:
               t5 = threading.Thread(target=shared_place.wworker, name="Writer 5")

            if randint(0, 6) % 2 == 0:
                t6 = threading.Thread(target=shared_place.rworker, name="Reader 6")
            else:
                t6 = threading.Thread(target=shared_place.wworker, name="Writer 6")

            if randint(0, 6) % 2 == 0:
                t7 = threading.Thread(target=shared_place.rworker, name="Reader 7")
            else:
                t7 = threading.Thread(target=shared_place.wworker, name="Writer 7")

            t1.start()
            t2.start()
            t3.start()
            t4.start()
            t5.start()
            t6.start()
            t7.start()

            t1.join()
            t2.join()
            t3.join()
            t4.join()
            t5.join()
            t6.join()
            t7.join()
            print("\n%%%%%%%%%%%%%%%%%%%% Simulate a draw of a dice %%%%%%%%%%%%%%%%%%%%\n")

    def shutdown(self):
        self.running = False

pass
