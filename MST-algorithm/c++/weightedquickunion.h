#ifndef WEIGHTEDQUICKUNION_H
#define WEIGHTEDQUICKUNION_H


class WeightedQuickUnion
{
public:
    WeightedQuickUnion();
    WeightedQuickUnion(int n);
    int* unionArray;
    int* unionSize;
    bool connected(int p, int q);
    int root(int p);
    void connect(int p, int q);
};

#endif // WEIGHTEDQUICKUNION_H
