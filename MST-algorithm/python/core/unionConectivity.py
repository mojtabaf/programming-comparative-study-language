class WeightedQuickUnion:
    _unionArray = None
    _unionSize = None

    def __init__(self, n):
        self._unionArray = []
        self._unionSize = []
        for i in range(n):
            self._unionArray.append(i)
            self._unionSize.append(1)
        return

    def connected(self, p, q):
        return self.root(p) == self.root(q)

    def root(self, p):
        while p != self._unionArray[p]:
            p = self._unionArray[p]
        return p

    def union(self, p, q):
        i = self.root(p)
        j = self.root(q)
        if i == j:
            return
        if self._unionSize[i] < self._unionSize[j]:
            self._unionArray[i] = j
            self._unionSize[j] += self._unionSize[i]
        else:
            self._unionArray[j] = i
            self._unionSize[i] += self._unionSize[j]

        return

pass
