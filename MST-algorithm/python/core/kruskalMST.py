from operator import itemgetter
from unionConectivity import WeightedQuickUnion
import time
import profiler


class MSTFinder:

    
    
    def __init__(self, path, output):
        self.path = path
        self.output = output
        self.start_time_memory = profiler.memory()
        self.app_start = time.time() * 1000 * 1000
        self.read_end = 0
        self.edges = []
        self.stepedQuickUnion = None
        self.vertices = None

    def setStepedQuickUnion( self, stepedQuickUnion):
        self.stepedQuickUnion = stepedQuickUnion

    def kruskal(self, nodes):
        mst = []
        quick_union = WeightedQuickUnion(nodes)
        for node in self.edges:
            v = node[0]
            w = node[1]
            if not quick_union.connected(v - 1, w - 1):
                quick_union.union(v - 1, w - 1)
                mst.append((v, w, node[2]))
        return mst

    def controlledKruskal(self):
        e = None
        if (len(self.edges) < 1):
            return None
        node = self.edges[0]
        v = node[0]
        w = node[1];
        if not self.stepedQuickUnion.connected(v - 1, w - 1):
            self.stepedQuickUnion.union(v - 1, w - 1)
            e = self.edges[0]
            self.edges.remove(e)
        return e

    def output_creator(self, output_path, string, calculation_end):
        with open(output_path, 'w') as file:
            file.write(string)
            write_end = time.time() * 1000 * 1000
            file.write("IO write time: " + str(write_end - calculation_end) + " nanosecond\n")
            file.write("Total execution time: " + str(write_end - self.app_start) + " nanosecond\n")
            end_time_memory = profiler.memory(self.start_time_memory)
            memory_consumption = end_time_memory / 1024.0
            file.write("Memory Consumption: " + str(memory_consumption) + " Kb");
            # end_time_memory = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
            #
        return

    def get_lines(self, file):
        for line in file:
            yield line

    def reader(self):
        with open(self.path, 'r') as f:
            index = 0
            i = 0
            for line in self.get_lines(f):
                line_content = line.split()
                j = 0
                for weight in line_content:
                    if j > index and int(weight) != 0:
                        self.edges.append((i + 1, j + 1, int(weight)))
                    j += 1
                index += 1
                i += 1
            read_end = time.time() * 1000 * 1000
            self.edges.sort(key=itemgetter(2))
            self.vertices = index
        return self.vertices, read_end

    def result_reader(self):
        with open(self.output, 'r') as f:
            lines = f.readlines()
            return lines

    def getMSTWeight(self,  mst):
        s = "( "
        sum = 0
        for i, edge in enumerate(mst):
            if i == len(mst) - 1:
                s = s + str(edge[2])
            else:
                s = s + str(edge[2]) + " + "
            sum += edge[2]
        s = s + ") = " + str(sum);
        return s

    def finder(self):
        readeObj = self.reader()
        vertices = readeObj[0]
        read_end = readeObj[1]
        msTree = self.kruskal(vertices)
        calculation_end = time.time() * 1000 * 1000
        file_content = "Total number of nodes = {0}\nTotal number of edges in the minimum spanning three = {1}\n".format(
            vertices, len(msTree))
        file_content += "List of edges & their costs:\n"
        mst_sum = 0
        sumation = "("
        i = 0
        for edge in msTree:
            if i == len(msTree) - 1:
                sumation += str(edge[2])
            else:
                sumation += str(edge[2]) + " + "
            mst_sum += edge[2]
            file_content += "({0}, {1})\tedge cost: {2}\n".format(edge[0], edge[1], edge[2])
            i += 1
        sumation += ") = {0}".format(mst_sum)
        file_content += "Total cost of minimum spanning three is = Sum of " + sumation + "\n"
        file_content += "IO read time: " + str(read_end - self.app_start) + " nanosecond\n";
        file_content += "Algorithm execution time: " + str(calculation_end - read_end) + " nanosecond\n";
        self.output_creator(self.output, file_content, calculation_end)
        return True

pass
