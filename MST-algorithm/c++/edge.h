#ifndef EDGE_H
#define EDGE_H
#include "string"
#include "cstdlib"
using namespace std;

class Edge
{
public:
    Edge(int v, int w, int weight);
    //string getString();
    int v;
    int w;
    int weight;
};


#endif // EDGE_H
