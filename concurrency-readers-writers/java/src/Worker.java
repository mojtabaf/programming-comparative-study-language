
public class Worker implements Runnable {

	private String workerType;
	private SharedPlaceController sharedPlace;
	public Worker(String workerType, SharedPlaceController sharedPlace) {
		this.workerType = workerType;
		this.sharedPlace = sharedPlace;
	}
	@Override
	public void run() {
		if(this.workerType.equalsIgnoreCase("writer")){
			final int DELAY = 3000;
			try {
				Thread.sleep((int) (Math.random() * DELAY));
			} catch (InterruptedException e) {
			}
			this.sharedPlace.write();
		}else{
			final int DELAY = 3000;
			try {
				Thread.sleep((int) (Math.random() * DELAY));
			} catch (InterruptedException e) {
			}
			this.sharedPlace.read();
		}
		
		
	}

}
