import java.util.Random;

public class Simulator extends Thread {
	private volatile boolean running = true;

	public void run() {
		Random random = new Random();
		SharedPlaceController sharedPlace = new SharedPlaceController();
		System.out.println("%%%%%%%%%%%%%%%%%%%% Simulate a draw of a dice %%%%%%%%%%%%%%%%%%%%\n");
		while (this.running) {
			Thread t1;
			Thread t2;
			Thread t3;
			Thread t4;
			Thread t5;
			Thread t6;
			Thread t7;
			if (random.nextInt(7) % 2 == 0) {
				t1 = new Thread(new Worker("reader", sharedPlace));
				t1.setName("Reader 1");
			} else {
				t1 = new Thread(new Worker("writer", sharedPlace));
				t1.setName("Writer 1");
			}
			if (random.nextInt(7) % 2 == 0) {
				t2 = new Thread(new Worker("reader", sharedPlace));
				t2.setName("Reader 2");
			} else {
				t2 = new Thread(new Worker("writer", sharedPlace));
				t2.setName("Writer 2");
			}
			if (random.nextInt(7) % 2 == 0) {
				t3 = new Thread(new Worker("reader", sharedPlace));
				t3.setName("Reader 3");
			} else {
				t3 = new Thread(new Worker("writer", sharedPlace));
				t3.setName("Writer 3");
			}
			if (random.nextInt(7) % 2 == 0) {
				t4 = new Thread(new Worker("reader", sharedPlace));
				t4.setName("Reader 4");
			} else {
				t4 = new Thread(new Worker("writer", sharedPlace));
				t4.setName("Writer 4");
			}
			if (random.nextInt(7) % 2 == 0) {
				t5 = new Thread(new Worker("reader", sharedPlace));
				t5.setName("Reader 5");
			} else {
				t5 = new Thread(new Worker("writer", sharedPlace));
				t5.setName("Writer 5");
			}
			if (random.nextInt(7) % 2 == 0) {
				t6 = new Thread(new Worker("reader", sharedPlace));
				t6.setName("Reader 6");
			} else {
				t6 = new Thread(new Worker("writer", sharedPlace));
				t6.setName("Writer 6");
			}
			if (random.nextInt(7) % 2 == 0) {
				t7 = new Thread(new Worker("reader", sharedPlace));
				t7.setName("Reader 7");
			} else {
				t7 = new Thread(new Worker("writer", sharedPlace));
				t7.setName("Writer 7");
			}

			t1.start();
			t2.start();
			t3.start();
			t4.start();
			t5.start();
			t6.start();
			t7.start();

			try {
				t1.join();
				t2.join();
				t3.join();
				t4.join();
				t5.join();
				t6.join();
				t7.join();
				System.out.println("\n%%%%%%%%%%%%%%%%%%%% Simulate a draw of a dice %%%%%%%%%%%%%%%%%%%%\n");
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public void shutdown() {
		this.running = false;
	}
}