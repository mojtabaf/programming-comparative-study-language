#pragma once
#include <vector>
#include <cstdlib>
#include <mutex>
#include <string>
#include <thread>
#include <condition_variable>
using namespace std;

class PlaceController
{
public:
	PlaceController();
	void read(string name);
	void reader(string name);
	void write(string name);
	void writer(string name);
	void worker();
	void shutdown();
private:
	int reader_number = 0;
	int list_limit = 10;
	vector<int> sharedList;
	mutex _mutex;
	condition_variable condition;
	volatile bool running = true;
	thread t1;
	thread t2;
	thread t3;
	thread t4;
	thread t5;
	thread t6;
	thread t7;
};
