﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Assignment3
{
    class Simulator
    {
        private volatile bool running = true;
        public void run()
        {
            var random = new Random();
            var sharedPlaceController = new SharedPlaceController();
            Console.WriteLine("%%%%%%%%%%%%%%%%%%%% Simulate a draw of a dice %%%%%%%%%%%%%%%%%%%%\n");
            while (this.running)
            {
                Thread t1;
                Thread t2;
                Thread t3;
                Thread t4;
                Thread t5;
                Thread t6;
                Thread t7;

                if (random.Next(7) % 2 == 0)
                {
                    t1 = new Thread(new ThreadStart(sharedPlaceController.readerRunner));
                    t1.Name = "Reader 1";
                }
                else {
                    t1 = new Thread(new ThreadStart(sharedPlaceController.writerRunner));
                    t1.Name = "Writer 1";
                }
                if (random.Next(7) % 2 == 0)
                {
                    t2 = new Thread(new ThreadStart(sharedPlaceController.readerRunner));
                    t2.Name = "Reader 2";
                }
                else {
                    t2 = new Thread(new ThreadStart(sharedPlaceController.writerRunner));
                    t2.Name = "Writer 2";
                }
                if (random.Next(7) % 2 == 0)
                {
                    t3 = new Thread(new ThreadStart(sharedPlaceController.readerRunner));
                    t3.Name = "Reader 3";
                }
                else {
                    t3 = new Thread(new ThreadStart(sharedPlaceController.writerRunner));
                    t3.Name = "Writer 3";
                }
                if (random.Next(7) % 2 == 0)
                {
                    t4 = new Thread(new ThreadStart(sharedPlaceController.readerRunner));
                    t4.Name = "Reader 4";
                }
                else {
                    t4 = new Thread(new ThreadStart(sharedPlaceController.writerRunner));
                    t4.Name = "Writer 4";
                }
                if (random.Next(7) % 2 == 0)
                {
                    t5 = new Thread(new ThreadStart(sharedPlaceController.readerRunner));
                    t5.Name = "Reader 5";
                }
                else {
                    t5 = new Thread(new ThreadStart(sharedPlaceController.writerRunner));
                    t5.Name = "Writer 5";
                }
                if (random.Next(7) % 2 == 0)
                {
                    t6 = new Thread(new ThreadStart(sharedPlaceController.readerRunner));
                    t6.Name = "Reader 6";
                }
                else {
                    t6 = new Thread(new ThreadStart(sharedPlaceController.writerRunner));
                    t6.Name = "Writer 6";
                }
                if (random.Next(7) % 2 == 0)
                {
                    t7 = new Thread(new ThreadStart(sharedPlaceController.readerRunner));
                    t7.Name = "Reader 7";
                }
                else {
                    t7 = new Thread(new ThreadStart(sharedPlaceController.writerRunner));
                    t7.Name = "Writer 7";
                }

                t1.Start();
                t2.Start();
                t3.Start();
                t4.Start();
                t5.Start();
                t6.Start();
                t7.Start();

                t1.Join();
                t2.Join();
                t3.Join();
                t4.Join();
                t5.Join();
                t6.Join();
                t7.Join();

                Console.WriteLine("\n%%%%%%%%%%%%%%%%%%%% Simulate a draw of a dice %%%%%%%%%%%%%%%%%%%%\n");
            }
        }
        public void shutdown()
        {
            this.running = false;
        }
    }
}
