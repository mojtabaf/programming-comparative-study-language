#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "vector"
#include "cstdlib"
#include "kruskalmst.h"


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_actionStep_By_Step_triggered();

    void on_actionOne_Time_Execution_triggered();

    void on_actionReset_Result_triggered();

    void on_actionShow_Result_triggered();

    void on_actionSelect_Result_Path_triggered();

    void on_actionShow_Graph_Info_triggered();

    void on_actionSelect_Graph_Path_triggered();

    void on_comboBox_algorithm_currentIndexChanged(int index);

    void on_btnOneStepExecute_released();

    void on_actionHelp_triggered();

private:
    Ui::MainWindow *ui;

    bool beginning = true;
    KruskalMST stepRunner;
    vector<Edge> mst;
};

#endif // MAINWINDOW_H
