import java.util.Scanner;

public class Program {

	public static void main(String[] args) {
		Simulator simulator = new Simulator();
		simulator.start();
		
		System.out.println("For Stopping the application press enter.....!");
		Scanner scanner = new Scanner(System.in);
		scanner.nextLine();
		simulator.shutdown();
		try {
			simulator.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("done!");
	}
}
