package kruskalsolution;

/**
 * Created by ARZ on 7/12/2016.
 */
public class Edge implements Comparable<Edge> {

    private final int v;
    private final int w;
    private final int weight;

    public Edge(int v, int w, int weight) {
        this.v = v;
        this.w = w;
        this.weight = weight;
    }

    @Override
    public int compareTo(Edge that) {
        return Integer.compare(this.weight, that.weight);
    }

    public double weight() {
        return weight;
    }
    public int either() {
        return v;
    }

    public int other(int vertex) {
        if(vertex == v)
            return w;
        else
            return v;
    }

    public String toString() {
        return "(" + v + ", " + w + ")" + "\tedge cost: " + weight;
    }

}
