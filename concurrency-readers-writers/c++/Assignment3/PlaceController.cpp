#include "stdafx.h"
#include "PlaceController.h"
#include <vector>
#include <cstdlib>
#include <mutex>
#include <condition_variable>
#include <string>
#include <thread>
#include <iostream>
#include <algorithm>
#include <sstream>
#include <ctime>
#include <chrono>

using namespace std;


PlaceController::PlaceController()
{
	this->reader_number = 0;
}

void PlaceController::read(string name){

	{
		lock_guard<mutex> lock(this->_mutex);
		this->reader_number++;
		string str = "Thread " + name + " now is ready to read the shared location.\t ";
		cout << str;
	}

	stringstream result;
	string str = "[]";
	if (this->sharedList.size() > 0){
		copy(this->sharedList.begin(), this->sharedList.end() - 1,
			ostream_iterator<int>(result, ", "));
		str = result.str() + to_string(this->sharedList.back());
		str = "[" + str + "]";
	}
	cout << str << endl;

	srand((int)time(0));
	int delay = (rand() % 3000);
	this_thread::sleep_for(chrono::milliseconds(delay));

	{
		lock_guard<mutex> lock(this->_mutex);
		string str = "Thread " + name + " now has finished reading the shared location.";
		cout << str << endl;
		this->reader_number--;
		if (this->reader_number == 0){
			this->condition.notify_all();
		}
	}
}

void PlaceController::write(string name){
	unique_lock<mutex> lock(this->_mutex);
	while (this->reader_number != 0)
	{
		this->condition.wait(lock);
	}

	string str = "Thread " + name + " now is ready to write the shared location.\t ";
	cout << str << endl;
	if (this->sharedList.size() > this->list_limit){
		this->sharedList.erase(this->sharedList.begin());
	}
	srand((int)time(0));
	int num = (rand() % 100);
	this->sharedList.push_back(num);

	srand((int)time(0));
	int delay = (rand() % 3000);
	this_thread::sleep_for(chrono::milliseconds(delay));
	string out = "Thread " + name + "  now has finished writing the shared location. " + to_string(num);
	cout << out << endl;
	this->condition.notify_all();

}

void PlaceController::reader(string name){
	srand((int)time(0));
	int delay = (rand() % 3000);
	this_thread::sleep_for(chrono::milliseconds(delay));
	this->read(name);
}

void PlaceController::writer(string name){
	srand((int)time(0));
	int delay = (rand() % 3000);
	this_thread::sleep_for(chrono::milliseconds(delay));
	this->write(name);
}

void PlaceController::worker(){
	cout << "\n*********************Dice Rolling Simulation*********************\n" << endl;
	while (this->running)
	{
		srand((int)time(0));
		int r = (rand() % 7 + 1);
		if (r % 2 != 0){

			this->t1 = thread(&PlaceController::reader, this, "Reader 1");
		}
		else{
			this->t1 = thread(&PlaceController::writer, this, "Writer 1");
		}

		r = (rand() % 7 + 1);
		if (r % 2 != 0){

			this->t2 = thread(&PlaceController::reader, this, "Reader 2");
		}
		else{
			this->t2 = thread(&PlaceController::writer, this, "Writer 2");
		}

		r = (rand() % 7 + 1);
		if (r % 2 != 0){

			this->t3 = thread(&PlaceController::reader, this, "Reader 3");
		}
		else{
			this->t3 = thread(&PlaceController::writer, this, "Writer 3");
		}

		r = (rand() % 7 + 1);
		if (r % 2 != 0){

			this->t4 = thread(&PlaceController::reader, this, "Reader 4");
		}
		else{
			this->t4 = thread(&PlaceController::writer, this, "Writer 4");
		}

		r = (rand() % 7 + 1);
		if (r % 2 != 0){

			this->t5 = thread(&PlaceController::reader, this, "Reader 5");
		}
		else{
			this->t5 = thread(&PlaceController::writer, this, "Writer 5");
		}

		r = (rand() % 7 + 1);
		if (r % 2 != 0){

			this->t6 = thread(&PlaceController::reader, this, "Reader 6");
		}
		else{
			this->t6 = thread(&PlaceController::writer, this, "Writer 6");
		}

		r = (rand() % 7 + 1);
		if (r % 2 != 0){
			this->t7 = thread(&PlaceController::reader, this, "Reader 7");
		}
		else{
			this->t7 = thread(&PlaceController::writer, this, "Writer 7");
		}

		t1.join();
		t2.join();
		t3.join();
		t4.join();
		t5.join();
		t6.join();
		t7.join();

		if (this->running){
			cout << "\n*********************Dice Rolling Simulation*********************\n" << endl;
		}
		else{
			cout << "#####################Application is Terminated#####################" << endl;
			cout << "please press \"Enter\" to close the terminal" << endl;
		}
		
	}
}

void PlaceController::shutdown(){
	this->running = false;
}

