#ifndef KRUSKALMST_H
#define KRUSKALMST_H
#include "cstdlib"
#include "vector"
#include "string"
#include "edge.h"
#include "weightedquickunion.h"

using namespace std;


class KruskalMST
{
public:
    KruskalMST();
    KruskalMST(string source, string result);
    //vector<Edge> getEdges();
    //void setStepedQuickUnion(WeightedQuickUnion stepedQuickUnion);
    //int getVertices();
    //void setVertices(int vertices);
    vector<Edge> kruskal();
    Edge ControlledKruskal();
    string getMSTWeight(vector<Edge> mst);
    void outputCreator(string path, string content);
    bool finder();
    int vertices;
    vector<Edge> edges;
    string source;
    string result;
    WeightedQuickUnion stepedQuickUnion;
};

#endif // KRUSKALMST_H
