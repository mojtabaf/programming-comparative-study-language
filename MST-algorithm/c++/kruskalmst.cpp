#include "kruskalmst.h"
#include "fstream"
#include "vector"
#include "string"
#include "regex"

using namespace std;

bool compare(const Edge&i, const Edge&j)
{
    return i.weight < j.weight;
}

KruskalMST::KruskalMST(){}
KruskalMST::KruskalMST(string source, string result)
{
    KruskalMST::result = result;
    KruskalMST::source = source;


    try{
        ifstream file(source);
        string line;
        int index  =  2;
        while (getline(file, line))
        {
            regex ws_re("\\s+");
            vector<std::string> strs{
                sregex_token_iterator(line.begin(), line.end(), ws_re, -1), {}
            };
            for (int i = index; i < strs.size(); i++) {
                if(stoi(strs[i]) != 0){
                    Edge e((index - 1), i, stoi(strs[i]));
                    KruskalMST::edges.push_back(e);
                }
            }
            index++;
        }
        KruskalMST::vertices = index - 2;
        //qsort((KruskalMST::edges.begin(), KruskalMST::edges.end(), (compfn)compare );
        sort(KruskalMST::edges.begin(), KruskalMST::edges.end(), compare);

      }catch (exception& e) {
              // e.printStackTrace();
      }

}

vector<Edge> KruskalMST::kruskal(){
        vector<Edge> mst;
        WeightedQuickUnion quickUnion(KruskalMST::vertices);
        for(Edge edge: KruskalMST::edges){
            int v = edge.v;
            int w = edge.w;
            if(!quickUnion.connected(v - 1, w - 1)){
                quickUnion.connect(v - 1, w - 1);
                mst.push_back(edge);
            }
        }
        return mst;
}

Edge KruskalMST::ControlledKruskal(){
        Edge e(0, 0, 0);
            if(this->edges.size() < 1){
                return e;
            }
            int v = ((Edge)this->edges[0]).v;
            int w = ((Edge)this->edges[0]).w;
            if(!(this->stepedQuickUnion.connected(v - 1, w - 1))){
                this->stepedQuickUnion.connect(v - 1, w - 1);
                e = this->edges[0];
                (this->edges).erase(this->edges.begin());
            }
        return e;
}

string KruskalMST::getMSTWeight(vector<Edge> mst){
        string str = "( ";
        int sum = 0;
        for(int i = 0; i < mst.size(); i++){
            if(i == mst.size() - 1){
                str += to_string(((Edge)mst[i]).weight);
            }else{
                str += to_string(((Edge)mst[i]).weight) + " + ";
            }
            sum += ((Edge)mst[i]).weight;
        }
        str += ") = " + to_string(sum);
        return str;
}

void KruskalMST::outputCreator(string path, string content){
   try {
        ofstream file;
        file.open(path);
        file << content;
        file.close();
   }catch (exception& e) {

   }
}

bool KruskalMST::finder() {
        vector<Edge> mst = KruskalMST::kruskal();

        string file_content = "Total number of nodes = " + to_string(KruskalMST::vertices) +"\n";
        file_content += "Total number of edges in the minimum spanning three = " + to_string(mst.size()) +"\n";
        file_content += "List of edges & their costs:\n";
        for(Edge edge: mst){
            int v = edge.v;
            int w = edge.w;
            int weight = edge.weight;
            file_content += "(" + to_string(v) + ", " + to_string(w) + ")" + "\tedge cost: " + to_string(weight) + "\n";
        }
        file_content += "Total cost of minimum spanning three is = Sum of "+ KruskalMST::getMSTWeight(mst) + "\n";
        KruskalMST::outputCreator(KruskalMST::result, file_content);
        return true;
}
