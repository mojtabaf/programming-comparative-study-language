// Assignment3.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "PlaceController.h"
#include <string>
#include <iostream>
#include <thread>
#include <cstdlib>

using namespace std;

PlaceController placeController;

void runner(){

	placeController.worker();
}

int _tmain(int argc, _TCHAR* argv[])
{

	cout << "Press Enter to Stop application!.............." << endl;
	thread th(&runner);
	cin.get();
	placeController.shutdown();
	th.join();
	cin.get();
	return 0;
}
