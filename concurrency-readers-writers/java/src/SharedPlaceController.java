import java.util.LinkedList;
import java.util.Random;


public class SharedPlaceController {
	private LinkedList<Integer> lst = new LinkedList<Integer>();
	 private int reader_count = 0;
     private int BOUND = 12;
     public SharedPlaceController()
     {
         this.reader_count = 0;
     }
	
	public void read() {
		synchronized (this) {
			 this.reader_count++;
             String thread_name = Thread.currentThread().getName();
             System.out.println("Thread " + thread_name + " now is ready to read  the shared location.\t ");
		}

		int delay_base = 3000;
		try {
//			System.out.println(lst.toString());
			Thread.sleep((int) (Math.random() * delay_base));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		synchronized (this) {
			String thread_name = Thread.currentThread().getName();
			System.out.println("Thread " + thread_name + " now has finished reading the shared location.");
			this.reader_count--;
			if (this.reader_count == 0) {
				this.notifyAll();
			}
		}
	}
	
	public synchronized void write() {
		while (this.reader_count != 0) {
			try {
				this.wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		String thread_name = Thread.currentThread().getName();
		System.out.println("Thread " + thread_name + " now is ready to write the shared location.\t ");
		Random random = new Random();
		if(lst.size() > this.BOUND){
			lst.removeFirst();
		}
		int number = random.nextInt(100);
		lst.addLast(number);

		int delay_base = 3000;
		try {
			Thread.sleep((int) (Math.random() * delay_base));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		System.out.println("Thread " + thread_name + "  now has finished writing the shared location. "); // + number);
		this.notifyAll();
	}
}