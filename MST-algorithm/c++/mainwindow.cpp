#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "QMessageBox"
#include "QFileDialog"
#include "fstream"
#include "string"
#include "QFile"
#include "QTextStream"
#include "qdesktopservices.h"
#include "edge.h"
#include "weightedquickunion.h"
#include "kruskalmst.h"
#include "iostream"

using namespace std;


/*bool beginning = true;
KruskalMST stepRunner;
vector<Edge> mst*/;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->panel->setVisible(false);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionStep_By_Step_triggered()
{
    ui->panel->setVisible(true);
    ui->lblStatus->setText("Status: Ready to Start!");

}

void MainWindow::on_actionOne_Time_Execution_triggered()
{
    ui->panel->setVisible(false);
    if(ui->lblPath->text() == "" || ui->lblResult->text() == ""){
        QMessageBox::critical(this, tr("Message"),
                                       tr("please select source and result path!"),
                                       QMessageBox::Ok);

    }else{
        ui->lblStatus->setText("Status: Running!");
        KruskalMST k(ui->lblPath->text().toStdString(), ui->lblResult->text().toStdString());
        if(k.finder()){
            ui->lblStatus->setText("Status: Completed!");
            //"(" + to_string(Edge::v) + ", " + to_string(Edge::w) + ")" + "\tedge cost: " + to_string(Edge::weight);
        }
    }

}

void MainWindow::on_actionReset_Result_triggered()
{
    ui->lblStatus->setText("Status: Ready to Start!");
    ui->comboBox_algorithm->setCurrentIndex(0);
    ui->comboBox_steps->setCurrentIndex(0);
    ui->textBrowser->setText("");
    beginning = true;
    ui->btnOneStepExecute->setEnabled(true);
    vector<Edge> m;
    mst = m;
    KruskalMST k;
    stepRunner = k;
}

void MainWindow::on_actionShow_Result_triggered()
{
    try{
        QFile file(ui->lblResult->text());
        if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
           QMessageBox::critical(this, tr("Error"), tr("can not open te result file"), QMessageBox::Ok);

        ui->textBrowser->setText("");
        QTextStream stream(&file);
        while (!stream.atEnd()) {
            QString line = stream.readLine();
            ui->textBrowser->append(line);
        }
        file.close();

    }catch(exception& exp){
        QMessageBox::critical(this, tr("Error"), tr(exp.what()), QMessageBox::Ok);
    }

}

void MainWindow::on_actionSelect_Result_Path_triggered()
{
    auto fName = QFileDialog::getSaveFileName(this, tr("Select a place where you want to save the result"), QString(), tr("*.txt"));
    ui->lblResult->setText(fName);

}

void MainWindow::on_actionShow_Graph_Info_triggered()
{
    try{
        QFile file(ui->lblPath->text());
        if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
           QMessageBox::critical(this, tr("Error"), tr("can not open the selected file"), QMessageBox::Ok);

        ui->textBrowser->setText("");
        QTextStream stream(&file);
        while (!stream.atEnd()) {
            QString line = stream.readLine();
            ui->textBrowser->append(line);
        }
        file.close();

    }catch(exception& exp){
        QMessageBox::critical(this, tr("Error"), tr(exp.what()), QMessageBox::Ok);
    }
}

void MainWindow::on_actionSelect_Graph_Path_triggered()
{
    auto fName = QFileDialog::getOpenFileName(this, tr("Select your graph information"), QString(), tr("*.txt"));
    ui->lblPath->setText(fName);

}

void MainWindow::on_comboBox_algorithm_currentIndexChanged(int index)
{
    if(index == 1){
        ui->btnOneStepExecute->setVisible(false);
        QMessageBox::information(this, tr("Message"),
                                       tr("Prim algorithm is not availble right now!"),
                                       QMessageBox::Ok);

    }else{
        ui->btnOneStepExecute->setVisible(true);
    }

}

void MainWindow::on_btnOneStepExecute_released()
{
    if(ui->lblPath->text() == "" || ui->lblResult->text() == ""){
        ui->textBrowser->setText("");
        ui->lblStatus->setText("Status: Ready to Start!");
        QMessageBox::critical(this, tr("Message"),
                                       tr("please select source and result path!"),
                                       QMessageBox::Ok);

    }else{
        ui->lblStatus->setText("Status: Running!");
        try {
            if(ui->comboBox_algorithm->currentIndex() < 0 || ui->comboBox_steps->currentIndex() < 0)
               {
                    QMessageBox::warning(this, tr("Message"),
                                               tr("Please select algorithm and running steps!"),
                                               QMessageBox::Ok);
                   return;
                }
            if (beginning)
            {;
                  ui->lblStatus->setText("Status: Running");
                  ui->textBrowser->setText("");
                  string source = ui->lblPath->text().toStdString();
                  string result = ui->lblResult->text().toStdString();
                  KruskalMST kMST(source, result);
                  stepRunner = kMST;
                  int vertices = stepRunner.vertices;
                  WeightedQuickUnion quick(vertices);
                  stepRunner.stepedQuickUnion = quick;
                  ui->textBrowser->append("Total number of nodes = " + QString::number(vertices) + "\n");
                  ui->textBrowser->append("Total number of edges in the minimum spanning three = " + QString::number(vertices - 1) + "\n");
                  ui->textBrowser->append("List of edges & their costs:\n");
                  beginning =  false;
            }
            auto index = 1;
            if(stepRunner.edges.size() >= 1){
                auto selectedItem = ui->comboBox_steps->currentIndex();
                switch (selectedItem)
                {
                    case 0:
                        index = 1;
                        break;
                    case 1:
                        index = 5;
                        break;
                    case 2:
                        index = 10;
                        break;
                    case 3:
                        index = 50;
                        break;
                    case 4:
                        index = 100;
                        break;
                    default:
                        break;
                }
                for(int i = 0; i < index; i++){
                    Edge edge= stepRunner.ControlledKruskal();
                    if(!(edge.v == 0 && edge.w == 0 && edge.weight == 0)){
                        ui->textBrowser->append("(" + QString::number(edge.v) + ", " + QString::number(edge.w) + ")" + "\tedge cost: " + QString::number(edge.weight) + "\n");
                        mst.push_back(edge);
                    }
                    else
                    {
                        if(stepRunner.edges.size() >= 1){
                            Edge node = stepRunner.edges[0];
                            ui->textBrowser->append("(" + QString::number(node.v) + ", " + QString::number(node.w) + ")" + "\tedge cost: " + QString::number(node.weight)
                                                    + "\t\t******* Cycle happend *******\n");
                            stepRunner.edges.erase(stepRunner.edges.begin());
                        }

                    }
                    if(stepRunner.edges.size() < 1){
                        ui->textBrowser->append("Total cost of minimum spanning three is = Sum of ");
                        QString s = QString::fromStdString(stepRunner.getMSTWeight(mst));
                        ui->textBrowser->append(s);
                        ui->btnOneStepExecute->setEnabled(false);
                        ui->lblStatus->setText("Status: Completed!");
                        KruskalMST k;
                        stepRunner = k;
                        break;

                    }
                }
            }
        } catch (...) {
        }
    }

}

void MainWindow::on_actionHelp_triggered()
{

   QDesktopServices::openUrl(QUrl::fromLocalFile(QDir::currentPath()+"/Help.pdf"));
}
