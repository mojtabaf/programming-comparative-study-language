
class Reader(val placeController: PlaceController) extends Runnable{

  override def run(): Unit = {
    Thread.sleep((Math.random() * 3000).toLong)
    this.placeController.read()
  }
}
