#include "weightedquickunion.h"
#include "cstdlib"
#include "exception"
#include "string"
using namespace std;

WeightedQuickUnion::WeightedQuickUnion(){}
WeightedQuickUnion::WeightedQuickUnion(int n)
{
    try {
        unionArray = new int[n];
        unionSize = new int[n];
        for(int i = 0;i < n; i++){
            unionArray[i] = i;
            unionSize[i] = 1;
        }
    } catch (exception e) {
        //throw exception("sssss");
    }
}

bool WeightedQuickUnion::connected(int p, int q)
{
   return WeightedQuickUnion::root(p) == WeightedQuickUnion::root(q);
}

int WeightedQuickUnion::root(int p)
{
        try {
            while (p != WeightedQuickUnion::unionArray[p]) {
                p = WeightedQuickUnion::unionArray[p];
            }
        } catch (exception& e) {
           // System.out.println("QuickUnion--> root method"+e.getMessage());
        }
        return p;
 }

void WeightedQuickUnion::connect(int p, int q)
{
        try {
            int i = WeightedQuickUnion::root(p);
            int j = WeightedQuickUnion::root(q);
            if( i == j) return;
            if(WeightedQuickUnion::unionSize[i] < WeightedQuickUnion::unionSize[j]){
                WeightedQuickUnion::unionArray[i] = j;
                WeightedQuickUnion::unionSize[j] += WeightedQuickUnion::unionSize[i];
            }else{
                WeightedQuickUnion::unionArray[j] = i;
                WeightedQuickUnion::unionSize[i] += WeightedQuickUnion::unionSize[j];
            }
        } catch (exception& e) {
            //System.out.println("QuickUnion--> union method"+e.getMessage());
        }

}
