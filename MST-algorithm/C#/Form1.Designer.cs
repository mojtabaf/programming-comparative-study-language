﻿namespace Assignment2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.selectSourceFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.selectGraphToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showGraphInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.selectResultPathToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.selectResultPathToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.showResultToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.runToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kruskalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pauseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resumeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lable1 = new System.Windows.Forms.Label();
            this.labale2 = new System.Windows.Forms.Label();
            this.lblSource = new System.Windows.Forms.Label();
            this.lblResult = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.lblStatus = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.panel_steps = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.btnStepsExec = new System.Windows.Forms.Button();
            this.comboBox_steps = new System.Windows.Forms.ComboBox();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.panel_steps.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.selectSourceFileToolStripMenuItem,
            this.selectResultPathToolStripMenuItem,
            this.runToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(5, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(1065, 28);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "Selected Graph Path:";
            // 
            // selectSourceFileToolStripMenuItem
            // 
            this.selectSourceFileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.selectGraphToolStripMenuItem,
            this.showGraphInfoToolStripMenuItem});
            this.selectSourceFileToolStripMenuItem.Name = "selectSourceFileToolStripMenuItem";
            this.selectSourceFileToolStripMenuItem.Size = new System.Drawing.Size(66, 24);
            this.selectSourceFileToolStripMenuItem.Text = "Source";
            // 
            // selectGraphToolStripMenuItem
            // 
            this.selectGraphToolStripMenuItem.Name = "selectGraphToolStripMenuItem";
            this.selectGraphToolStripMenuItem.Size = new System.Drawing.Size(201, 26);
            this.selectGraphToolStripMenuItem.Text = "Select Graph Path";
            this.selectGraphToolStripMenuItem.Click += new System.EventHandler(this.selectGraphToolStripMenuItem_Click);
            // 
            // showGraphInfoToolStripMenuItem
            // 
            this.showGraphInfoToolStripMenuItem.Name = "showGraphInfoToolStripMenuItem";
            this.showGraphInfoToolStripMenuItem.Size = new System.Drawing.Size(201, 26);
            this.showGraphInfoToolStripMenuItem.Text = "Show Graph Info";
            this.showGraphInfoToolStripMenuItem.Click += new System.EventHandler(this.showGraphInfoToolStripMenuItem_Click);
            // 
            // selectResultPathToolStripMenuItem
            // 
            this.selectResultPathToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.selectResultPathToolStripMenuItem1,
            this.showResultToolStripMenuItem});
            this.selectResultPathToolStripMenuItem.Name = "selectResultPathToolStripMenuItem";
            this.selectResultPathToolStripMenuItem.Size = new System.Drawing.Size(61, 24);
            this.selectResultPathToolStripMenuItem.Text = "Result";
            // 
            // selectResultPathToolStripMenuItem1
            // 
            this.selectResultPathToolStripMenuItem1.Name = "selectResultPathToolStripMenuItem1";
            this.selectResultPathToolStripMenuItem1.Size = new System.Drawing.Size(201, 26);
            this.selectResultPathToolStripMenuItem1.Text = "Select Result Path";
            this.selectResultPathToolStripMenuItem1.Click += new System.EventHandler(this.selectResultPathToolStripMenuItem1_Click);
            // 
            // showResultToolStripMenuItem
            // 
            this.showResultToolStripMenuItem.Name = "showResultToolStripMenuItem";
            this.showResultToolStripMenuItem.Size = new System.Drawing.Size(201, 26);
            this.showResultToolStripMenuItem.Text = "show Result";
            this.showResultToolStripMenuItem.Click += new System.EventHandler(this.showResultToolStripMenuItem_Click);
            // 
            // runToolStripMenuItem
            // 
            this.runToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.kruskalToolStripMenuItem,
            this.pauseToolStripMenuItem,
            this.resumeToolStripMenuItem});
            this.runToolStripMenuItem.Name = "runToolStripMenuItem";
            this.runToolStripMenuItem.Size = new System.Drawing.Size(46, 24);
            this.runToolStripMenuItem.Text = "Run";
            // 
            // kruskalToolStripMenuItem
            // 
            this.kruskalToolStripMenuItem.Name = "kruskalToolStripMenuItem";
            this.kruskalToolStripMenuItem.Size = new System.Drawing.Size(216, 26);
            this.kruskalToolStripMenuItem.Text = "One Time Execution";
            this.kruskalToolStripMenuItem.Click += new System.EventHandler(this.kruskalToolStripMenuItem_Click);
            // 
            // pauseToolStripMenuItem
            // 
            this.pauseToolStripMenuItem.Name = "pauseToolStripMenuItem";
            this.pauseToolStripMenuItem.Size = new System.Drawing.Size(216, 26);
            this.pauseToolStripMenuItem.Text = "Step By Step";
            this.pauseToolStripMenuItem.Click += new System.EventHandler(this.stepsToolStripMenuItem_Click);
            // 
            // resumeToolStripMenuItem
            // 
            this.resumeToolStripMenuItem.Name = "resumeToolStripMenuItem";
            this.resumeToolStripMenuItem.Size = new System.Drawing.Size(216, 26);
            this.resumeToolStripMenuItem.Text = "Reset Result";
            this.resumeToolStripMenuItem.Click += new System.EventHandler(this.resetToolStripMenuItem_Click);
            // 
            // lable1
            // 
            this.lable1.AutoSize = true;
            this.lable1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lable1.Location = new System.Drawing.Point(12, 62);
            this.lable1.Name = "lable1";
            this.lable1.Size = new System.Drawing.Size(149, 19);
            this.lable1.TabIndex = 1;
            this.lable1.Text = "Selected Graph Path:";
            // 
            // labale2
            // 
            this.labale2.AutoSize = true;
            this.labale2.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labale2.Location = new System.Drawing.Point(12, 126);
            this.labale2.Name = "labale2";
            this.labale2.Size = new System.Drawing.Size(144, 19);
            this.labale2.TabIndex = 2;
            this.labale2.Text = "Selected Result Path";
            // 
            // lblSource
            // 
            this.lblSource.AutoSize = true;
            this.lblSource.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSource.Location = new System.Drawing.Point(35, 91);
            this.lblSource.Name = "lblSource";
            this.lblSource.Size = new System.Drawing.Size(0, 19);
            this.lblSource.TabIndex = 3;
            // 
            // lblResult
            // 
            this.lblResult.AutoSize = true;
            this.lblResult.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblResult.Location = new System.Drawing.Point(35, 167);
            this.lblResult.Name = "lblResult";
            this.lblResult.Size = new System.Drawing.Size(0, 19);
            this.lblResult.TabIndex = 4;
            // 
            // textBox1
            // 
            this.textBox1.AcceptsTab = true;
            this.textBox1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.textBox1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(15, 252);
            this.textBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBox1.Size = new System.Drawing.Size(1031, 510);
            this.textBox1.TabIndex = 5;
            this.textBox1.WordWrap = false;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.lblStatus.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus.Location = new System.Drawing.Point(853, 11);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(160, 19);
            this.lblStatus.TabIndex = 6;
            this.lblStatus.Text = "Status: Ready To Start!";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // panel_steps
            // 
            this.panel_steps.Controls.Add(this.label2);
            this.panel_steps.Controls.Add(this.label1);
            this.panel_steps.Controls.Add(this.comboBox1);
            this.panel_steps.Controls.Add(this.btnStepsExec);
            this.panel_steps.Controls.Add(this.comboBox_steps);
            this.panel_steps.Location = new System.Drawing.Point(39, 202);
            this.panel_steps.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel_steps.Name = "panel_steps";
            this.panel_steps.Size = new System.Drawing.Size(979, 44);
            this.panel_steps.TabIndex = 7;
            this.panel_steps.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(331, 20);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 19);
            this.label2.TabIndex = 4;
            this.label2.Text = "Steps";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(16, 20);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 19);
            this.label1.TabIndex = 3;
            this.label1.Text = "Algorithm";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Kruskal",
            "Prim"});
            this.comboBox1.Location = new System.Drawing.Point(104, 15);
            this.comboBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(160, 24);
            this.comboBox1.TabIndex = 2;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // btnStepsExec
            // 
            this.btnStepsExec.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStepsExec.Location = new System.Drawing.Point(667, 15);
            this.btnStepsExec.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnStepsExec.Name = "btnStepsExec";
            this.btnStepsExec.Size = new System.Drawing.Size(100, 28);
            this.btnStepsExec.TabIndex = 1;
            this.btnStepsExec.Text = "Execute";
            this.btnStepsExec.UseVisualStyleBackColor = true;
            this.btnStepsExec.Click += new System.EventHandler(this.btnStepsExec_Click);
            // 
            // comboBox_steps
            // 
            this.comboBox_steps.FormattingEnabled = true;
            this.comboBox_steps.Items.AddRange(new object[] {
            "1 Step",
            "5 Steps",
            "10 Steps",
            "50 Steps",
            "100 Steps"});
            this.comboBox_steps.Location = new System.Drawing.Point(384, 16);
            this.comboBox_steps.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.comboBox_steps.Name = "comboBox_steps";
            this.comboBox_steps.Size = new System.Drawing.Size(160, 24);
            this.comboBox_steps.TabIndex = 0;
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.helpToolStripMenuItem1});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(53, 24);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // helpToolStripMenuItem1
            // 
            this.helpToolStripMenuItem1.Name = "helpToolStripMenuItem1";
            this.helpToolStripMenuItem1.Size = new System.Drawing.Size(181, 26);
            this.helpToolStripMenuItem1.Text = "Help";
            this.helpToolStripMenuItem1.Click += new System.EventHandler(this.helpToolStripMenuItem1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.LightBlue;
            this.ClientSize = new System.Drawing.Size(1065, 775);
            this.Controls.Add(this.panel_steps);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.lblResult);
            this.Controls.Add(this.lblSource);
            this.Controls.Add(this.labale2);
            this.Controls.Add(this.lable1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Form1";
            this.Text = "Minimum Spanning Tree";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panel_steps.ResumeLayout(false);
            this.panel_steps.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem selectSourceFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem selectResultPathToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem selectGraphToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showGraphInfoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem selectResultPathToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem showResultToolStripMenuItem;
        private System.Windows.Forms.Label lable1;
        private System.Windows.Forms.Label labale2;
        private System.Windows.Forms.Label lblSource;
        private System.Windows.Forms.Label lblResult;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.ToolStripMenuItem runToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem kruskalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pauseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resumeToolStripMenuItem;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Panel panel_steps;
        private System.Windows.Forms.Button btnStepsExec;
        private System.Windows.Forms.ComboBox comboBox_steps;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem1;
    }
}

