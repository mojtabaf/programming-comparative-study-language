import java.util.Scanner

import scala.util.Random

object AppRunner {

  class Simulator extends Thread{
    var running = true
    override def run(): Unit = {
      val random  = new Random()
      val placeController = new PlaceController()
      println("\n*********************Dice Rolling Simulation*********************\n")
      while(this.running){
        var t1 = new Thread
        var t2 = new Thread
        var t3 = new Thread
        var t4 = new Thread
        var t5 = new Thread
        var t6 = new Thread
        var t7 = new Thread

        if(random.nextInt(7) % 2 == 0){
          t1 = new Thread(new Reader(placeController))
          t1.setName("Reader 1")
        }else{
          t1 = new Thread(new Writer(placeController))
          t1.setName("Writer 1")
        }
        if (random.nextInt(7) % 2 == 0) {
          t2 = new Thread(new Reader(placeController))
          t2.setName("Reader 2")
        } else {
          t2 = new Thread(new Writer(placeController))
          t2.setName("Writer 2")
        }
        if (random.nextInt(7) % 2 == 0) {
          t3 = new Thread(new Reader(placeController))
          t3.setName("Reader 3")
        } else {
          t3 = new Thread(new Writer(placeController))
          t3.setName("Writer 3")
        }
        if (random.nextInt(7) % 2 == 0) {
          t4 = new Thread(new Reader(placeController))
          t4.setName("Reader 4")
        } else {
          t4 = new Thread(new Writer(placeController))
          t4.setName("Writer 4")
        }
        if (random.nextInt(7) % 2 == 0) {
          t5 = new Thread(new Reader(placeController))
          t5.setName("Reader 5")
        } else {
          t5 = new Thread(new Writer(placeController))
          t5.setName("Writer 5")
        }
        if (random.nextInt(7) % 2 == 0) {
          t6 = new Thread(new Reader(placeController))
          t6.setName("Reader 6")
        } else {
          t6 = new Thread(new Writer(placeController))
          t6.setName("Writer 6")
        }
        if (random.nextInt(7) % 2 == 0) {
          t7 = new Thread(new Reader(placeController))
          t7.setName("Reader 7")
        } else {
          t7 = new Thread(new Writer(placeController))
          t7.setName("Writer 7")
        }

        t1.start()
        t2.start()
        t3.start()
        t4.start()
        t5.start()
        t6.start()
        t7.start()

        t1.join()
        t2.join()
        t3.join()
        t4.join()
        t5.join()
        t6.join()
        t7.join()

        if (this.running){
          println("\n*********************Dice Rolling Simulation*********************\n")
        }
        else{
          println("\n#####################Application is Terminated#####################\n")
        }
      }
    }

    def shutdown() :Unit ={
      this.running = false
    }
  }

  def main(args: Array[String]): Unit = {
    println("Press Enter to Stop application!..............")
    val simulator = new Simulator()
    simulator.start()
    val scanner = new Scanner(System.in)
    scanner.nextLine()
    simulator.shutdown()
    simulator.join()
  }
}
