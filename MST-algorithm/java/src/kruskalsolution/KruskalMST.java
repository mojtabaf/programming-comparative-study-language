package kruskalsolution;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ARZ on 7/12/2016.
 */
public class KruskalMST {

    private int vertices;
    private List<Edge> edges = new ArrayList<>();
    public List<Edge> getEdges() {
		return edges;
	}
	private long app_start;
    private long read_end;
    private long calculation_end;
    private String source;
    private String result;
    private WeightedQuickUnion stepedQuickUnion;
    public void setStepedQuickUnion(WeightedQuickUnion stepedQuickUnion) {
		this.stepedQuickUnion = stepedQuickUnion;
	}
	//private List<Edge> stepByStepMST= new ArrayList<Edge>();
    public int getVertices() {
		return vertices;
	}
	private void setVertices(int vertices) {
		this.vertices = vertices;
	}
	public KruskalMST(String source, String result){
        this.app_start = System.nanoTime();
        this.result = result;
        this.source = source;
        try (BufferedReader br = new BufferedReader(new FileReader(this.source))) {
            String line;
            int index  =  2;
            while ((line = br.readLine()) != null) {
                String[] strs = line.split("    ");
                for (int i = index; i < strs.length; i++) {
                    if(Integer.parseInt(strs[i]) != 0){
                        Edge e = new Edge(index - 1, i, Integer.parseInt(strs[i]));
                        this.edges.add(e);
                    }
                }
                index++;
            }
            this.read_end = System.nanoTime();
            this.setVertices(index - 2);
            Collections.sort(this.edges);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public List<Edge> kruskal(){
        List<Edge> mst = new ArrayList<>();
        WeightedQuickUnion quickUnion = new WeightedQuickUnion(this.getVertices());
        for(Edge edge: this.edges){
            int v = edge.either();
            int w = edge.other(v);
            if(!quickUnion.connected(v - 1, w - 1)){
                quickUnion.union(v - 1, w - 1);
                mst.add(edge);
            }
        }
        this.calculation_end = System.nanoTime();
        return mst;
    }
    public Edge ControlledKruskal(){
    	Edge e = null;
        	if(this.edges.size() < 1){
        		return e;
        	}
            int v = this.edges.get(0).either();
            int w = this.edges.get(0).other(v);
            if(!this.stepedQuickUnion.connected(v - 1, w - 1)){
            	this.stepedQuickUnion.union(v - 1, w - 1);
            	e = this.edges.get(0);
            	this.edges.remove(0);
            }
        return e;
    }
    public String getMSTWeight(List<Edge> mst){
        String str = "( ";
        int sum = 0;
        for(int i = 0; i < mst.size(); i++){
            if(i == mst.size() - 1){
                str += mst.get(i).weight();
            }else{
                str += mst.get(i).weight() + " + ";
            }
            sum += mst.get(i).weight();
        }
        str += ") = " + sum;
        return str;
    }
    public void outputCreator(String path, String content, long start_time_memory){
        try {
            File file = new File(path);
            if (!file.exists()) {
                file.createNewFile();
            }

            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(content);
            long write_end = System.nanoTime();
            bw.append("IO write time: " + (write_end - this.calculation_end) + " nanosecond\n");
            bw.append("Total execution time: " + (write_end - this.app_start) + " nanosecond\n");
            long end_time_memory = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
            bw.append("Memory Consumption: " + ((end_time_memory - start_time_memory) / 1024)  + " Kb");
            bw.close();
        }catch (IOException e) {
            e.printStackTrace();
        }
    }
    public boolean finder() {
        long start_time_memory = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
        List<Edge> mst = kruskal();

        String file_content = "Total number of nodes = " + "" + this.getVertices() +"\n";
        file_content += "Total number of edges in the minimum spanning three = " + "" + mst.size() +"\n";
        file_content += "List of edges & their costs:\n";
        for(Edge edge: mst){
            file_content += edge.toString() + "\n";
        }
        file_content += "Total cost of minimum spanning three is = Sum of "+ getMSTWeight(mst) + "\n";
        file_content += "IO read time: " + (this.read_end - this.app_start) + " nanosecond\n";
        file_content += "kruskal's algorithm execution time: " + (this.calculation_end - this.read_end) + " nanosecond\n";
        this.outputCreator(this.result, file_content, start_time_memory);
        return true;
    }
}
