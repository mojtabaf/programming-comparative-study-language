﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Assignment3
{
    class SharedPlaceController
    {
        private int reader_count = 0;
        private LinkedList<int> sharedList = new LinkedList<int>();
        private int BOUND = 12;
        public SharedPlaceController()
        {
            this.reader_count = 0;
        }

        public void read()
        {
            lock (this)
            {
                this.reader_count++;
                var thread_name = Thread.CurrentThread.Name;
                Console.Write("Thread " + thread_name + " now is ready to read  the shared location.\t ");
            }
            var delay_base = 3000;
            var str = string.Join(", ", this.sharedList.ToArray());
            //Console.WriteLine("[" + str + "]");
            var random = new Random();
            var Delay = random.Next(delay_base);
            Thread.Sleep(Delay);

            lock (this)
            {
                var thread_name = Thread.CurrentThread.Name;
                Console.WriteLine("Thread " + thread_name + " now has finished reading the shared location.");
                this.reader_count--;
                if(this.reader_count == 0)
                {
                    Monitor.PulseAll(this);
                }
            }
        }

        public void readerRunner()
        {
            var delay_base = 3000;
            Thread.Sleep(new Random().Next(delay_base));
            this.read();
        }

        //[MethodImpl(MethodImplOptions.Synchronized)]
        public void write()
        {
            lock (this)
            {
                while (this.reader_count != 0)
                {
                    Monitor.Wait(this);
                }
                var thread_name = Thread.CurrentThread.Name;
                Console.WriteLine("Thread " + thread_name + " now is ready to write the shared location.\t ");
                if (this.sharedList.Count > this.BOUND)
                {
                    this.sharedList.RemoveFirst();
                }
                var random = new Random();
                var number = random.Next(100);
                this.sharedList.AddLast(number);
                var delay_base = 3000;
                var Delay = random.Next(delay_base);
                Thread.Sleep(Delay);
                Console.WriteLine("Thread " + thread_name + "  now has finished writing the shared location. ");// + number);
                Monitor.PulseAll(this);
            }
           
        }
        public void writerRunner()
        {
            var delay_base = 3000;
            Thread.Sleep(new Random().Next(delay_base));
            this.write();
        }
    }
}
