﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment2
{
    class KruskalMST
    {
        private int vertices;
        public List<Edge> edges = new List<Edge>();
        private static Stopwatch app_start = new Stopwatch();
        private static long execution_time;
        private static Stopwatch read_start = new Stopwatch();
        private static long io_read_time; 
        private static Stopwatch algorithm_start = new Stopwatch();
        private static long algorithm_time;
        private static Stopwatch write_start = new Stopwatch();
        private static long io_write_time;
        private string source;
        private string result;
        private WeightedQuickUnion quickUnion;

        public void setQuickUnion(WeightedQuickUnion quickUnion)
        {
            this.quickUnion = quickUnion;
        }
        //private List<Edge> stepByStepMST= new ArrayList<Edge>();
        public int getVertices()
        {
            return vertices;
        }
        private void setVertices(int vertices)
        {
            this.vertices = vertices;
        }

        public Edge ControlledKruskal()
        {
            Edge e = null;
            if (this.edges.Count < 1)
            {
                return e;
            }
            int v = this.edges[0].either();
            int w = this.edges[0].other(v);
            if (!this.quickUnion.connected(v - 1, w - 1))
            {
                this.quickUnion.union(v - 1, w - 1);
                e = this.edges[0];
                this.edges.Remove(e);
            }
            return e;
        }

        public KruskalMST(string source, string result)
        {
            this.source = source;
            this.result = result;
            try
            {
                List<string> list = new List<string>();
                using (var reader = new StreamReader(this.source))
                {
                    string line;
                    int index = 2;
                    while ((line = reader.ReadLine()) != null)
                    {
                        var strs = System.Text.RegularExpressions.Regex.Split(line, @"\s{4,}");
                        for (int i = index; i < strs.Length; i++)
                        {
                            if (Int32.Parse(strs[i]) != 0)
                            {
                                var e = new Edge(index - 1, i, Int32.Parse(strs[i]));
                                this.edges.Add(e);
                            }
                        }
                        index++;
                    }
                    read_start.Stop();
                    io_read_time = read_start.Elapsed.Milliseconds * 1000000;
                    algorithm_start.Start();
                    this.vertices = index - 2;
                    this.edges.Sort();
                }
            }
            catch (IOException e)
            {
                throw new Exception("can not open the file --> " + e.Message);
            }
            
        }
        public List<Edge> kruskal()
        {
            var mst = new List<Edge>();
            var quickUnion = new WeightedQuickUnion(this.vertices);
            foreach (var edge in this.edges)
            {
                var v = edge.either();
                var w = edge.other(v);
                if (!quickUnion.connected(v - 1, w - 1))
                {
                    quickUnion.union(v - 1, w - 1);
                    mst.Add(edge);
                }
            }
            return mst;
        }

        public string getMSTWeight(List<Edge> mst)
        {
            var str = "( ";
            var sum = 0;
            for (var i = 0; i < mst.Count; i++)
            {
                if (i == mst.Count - 1)
                {
                    str += mst[i].getWeight();
                }
                else {
                    str += mst[i].getWeight() + " + ";
                }
                sum += mst[i].getWeight();
            }
            str += ") = " + sum;
            return str;
        }

        public void outputCreator(String path, String content, long start_time_memory)
        {
            try
            {
                using (StreamWriter writer = new StreamWriter(path))
                {
                    writer.WriteLine(content);
                    write_start.Stop();
                    io_write_time = write_start.Elapsed.Milliseconds * 1000000;
                    app_start.Stop();
                    execution_time = app_start.Elapsed.Milliseconds * 1000000;
                    //long write_end = System.nanoTime();
                    writer.WriteLine("IO write time: " + io_write_time + " nanosecond");
                    writer.WriteLine("Total execution time: " + execution_time + " nanosecond");
                    var proc = Process.GetCurrentProcess();
                    var memory_consumption = proc.PrivateMemorySize64;
                    writer.WriteLine("Memory Consumption: " + (memory_consumption / 1024) + " Kb");
                }
            }
            catch (IOException e)
            {
                throw new Exception("can not write in to the file --> " + e.Message);
            }
        }

        public bool finder()
        {
            app_start.Start();
            read_start.Start();
            var mst = kruskal();
            algorithm_start.Stop();
            algorithm_time = algorithm_start.Elapsed.Milliseconds * 1000000;
            write_start.Start();
            string file_content = "Total number of nodes = " + "" + vertices + "\n";
            file_content += "Total number of edges in the minimum spanning three = " + "" + mst.Count + "\n";
            file_content += "List of edges & their costs:\n";
            foreach (var edge in mst)
            {
                file_content += edge.toString() + "\n";
            }
            file_content += "Total cost of minimum spanning three is = Sum of " + getMSTWeight(mst) + "\n";
            file_content += "IO read time: " + io_read_time + " nanosecond\n";
            file_content += "kruskal's algorithm execution time: " + algorithm_time + " nanosecond";
            outputCreator(this.result, file_content, 0);
            return true;
        }
    }
}

