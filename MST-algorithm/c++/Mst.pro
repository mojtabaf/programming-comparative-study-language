#-------------------------------------------------
#
# Project created by QtCreator 2016-07-26T16:51:47
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Mst
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    edge.cpp \
    weightedquickunion.cpp \
    kruskalmst.cpp

HEADERS  += mainwindow.h \
    edge.h \
    weightedquickunion.h \
    kruskalmst.h

FORMS    += mainwindow.ui

DISTFILES +=
