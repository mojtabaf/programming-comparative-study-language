# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/home/mojtaba/Documents/python/Assignment2/ui/mainwindow.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_mainWindow(object):
    def setupUi(self, mainWindow):
        mainWindow.setObjectName(_fromUtf8("mainWindow"))
        mainWindow.setEnabled(True)
        mainWindow.resize(962, 822)
        self.centralWidget = QtGui.QWidget(mainWindow)
        self.centralWidget.setObjectName(_fromUtf8("centralWidget"))
        self.gridLayout = QtGui.QGridLayout(self.centralWidget)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.textBrowser = QtGui.QTextBrowser(self.centralWidget)
        self.textBrowser.setObjectName(_fromUtf8("textBrowser"))
        self.gridLayout.addWidget(self.textBrowser, 2, 0, 1, 1)
        self.lblStatus = QtGui.QLabel(self.centralWidget)
        self.lblStatus.setObjectName(_fromUtf8("lblStatus"))
        self.gridLayout.addWidget(self.lblStatus, 3, 0, 1, 1)
        self.gridLayout_2 = QtGui.QGridLayout()
        self.gridLayout_2.setObjectName(_fromUtf8("gridLayout_2"))
        self.label_4 = QtGui.QLabel(self.centralWidget)
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.gridLayout_2.addWidget(self.label_4, 9, 1, 1, 1)
        self.label_2 = QtGui.QLabel(self.centralWidget)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.gridLayout_2.addWidget(self.label_2, 6, 1, 1, 1)
        self.label = QtGui.QLabel(self.centralWidget)
        self.label.setObjectName(_fromUtf8("label"))
        self.gridLayout_2.addWidget(self.label, 0, 1, 1, 1)
        self.label_3 = QtGui.QLabel(self.centralWidget)
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.gridLayout_2.addWidget(self.label_3, 7, 1, 1, 1)
        self.lblPath = QtGui.QLabel(self.centralWidget)
        self.lblPath.setText(_fromUtf8(""))
        self.lblPath.setObjectName(_fromUtf8("lblPath"))
        self.gridLayout_2.addWidget(self.lblPath, 4, 1, 1, 5)
        self.lbldestText = QtGui.QLabel(self.centralWidget)
        self.lbldestText.setObjectName(_fromUtf8("lbldestText"))
        self.gridLayout_2.addWidget(self.lbldestText, 3, 1, 1, 3)
        self.btnNavigate = QtGui.QPushButton(self.centralWidget)
        self.btnNavigate.setObjectName(_fromUtf8("btnNavigate"))
        self.gridLayout_2.addWidget(self.btnNavigate, 6, 5, 1, 1)
        self.lblResult = QtGui.QLabel(self.centralWidget)
        self.lblResult.setText(_fromUtf8(""))
        self.lblResult.setObjectName(_fromUtf8("lblResult"))
        self.gridLayout_2.addWidget(self.lblResult, 8, 1, 1, 5)
        self.comboBoxSteps = QtGui.QComboBox(self.centralWidget)
        self.comboBoxSteps.setObjectName(_fromUtf8("comboBoxSteps"))
        self.comboBoxSteps.addItem(_fromUtf8(""))
        self.comboBoxSteps.addItem(_fromUtf8(""))
        self.comboBoxSteps.addItem(_fromUtf8(""))
        self.comboBoxSteps.addItem(_fromUtf8(""))
        self.comboBoxSteps.addItem(_fromUtf8(""))
        self.gridLayout_2.addWidget(self.comboBoxSteps, 11, 3, 1, 1)
        self.btnShowResult = QtGui.QPushButton(self.centralWidget)
        self.btnShowResult.setObjectName(_fromUtf8("btnShowResult"))
        self.gridLayout_2.addWidget(self.btnShowResult, 9, 4, 1, 1)
        self.btnKruskal = QtGui.QPushButton(self.centralWidget)
        self.btnKruskal.setObjectName(_fromUtf8("btnKruskal"))
        self.gridLayout_2.addWidget(self.btnKruskal, 9, 3, 1, 1)
        self.comboBoxAlgorithm1 = QtGui.QComboBox(self.centralWidget)
        self.comboBoxAlgorithm1.setObjectName(_fromUtf8("comboBoxAlgorithm1"))
        self.comboBoxAlgorithm1.addItem(_fromUtf8(""))
        self.comboBoxAlgorithm1.addItem(_fromUtf8(""))
        self.gridLayout_2.addWidget(self.comboBoxAlgorithm1, 9, 2, 1, 1)
        self.btnRefresh = QtGui.QPushButton(self.centralWidget)
        self.btnRefresh.setObjectName(_fromUtf8("btnRefresh"))
        self.gridLayout_2.addWidget(self.btnRefresh, 11, 5, 1, 1)
        self.comboBoxAlgorithm = QtGui.QComboBox(self.centralWidget)
        self.comboBoxAlgorithm.setObjectName(_fromUtf8("comboBoxAlgorithm"))
        self.comboBoxAlgorithm.addItem(_fromUtf8(""))
        self.comboBoxAlgorithm.addItem(_fromUtf8(""))
        self.gridLayout_2.addWidget(self.comboBoxAlgorithm, 11, 2, 1, 1)
        self.btnOneTimeExecution = QtGui.QPushButton(self.centralWidget)
        self.btnOneTimeExecution.setObjectName(_fromUtf8("btnOneTimeExecution"))
        self.gridLayout_2.addWidget(self.btnOneTimeExecution, 11, 4, 1, 1)
        self.label_5 = QtGui.QLabel(self.centralWidget)
        self.label_5.setObjectName(_fromUtf8("label_5"))
        self.gridLayout_2.addWidget(self.label_5, 11, 1, 1, 1)
        self.browser = QtGui.QPushButton(self.centralWidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.browser.sizePolicy().hasHeightForWidth())
        self.browser.setSizePolicy(sizePolicy)
        self.browser.setMaximumSize(QtCore.QSize(16777215, 16777215))
        self.browser.setIconSize(QtCore.QSize(16, 16))
        self.browser.setObjectName(_fromUtf8("browser"))
        self.gridLayout_2.addWidget(self.browser, 3, 5, 1, 1)
        self.btnHelp = QtGui.QPushButton(self.centralWidget)
        self.btnHelp.setObjectName(_fromUtf8("btnHelp"))
        self.gridLayout_2.addWidget(self.btnHelp, 0, 5, 1, 1)
        self.gridLayout.addLayout(self.gridLayout_2, 1, 0, 1, 1)
        mainWindow.setCentralWidget(self.centralWidget)

        self.retranslateUi(mainWindow)
        QtCore.QMetaObject.connectSlotsByName(mainWindow)

    def retranslateUi(self, mainWindow):
        mainWindow.setWindowTitle(_translate("mainWindow", "Minimum Spanning Tree", None))
        self.lblStatus.setText(_translate("mainWindow", "Current Status: ", None))
        self.label_4.setText(_translate("mainWindow", "One Time Execution", None))
        self.label_2.setText(_translate("mainWindow", "Please enter your result path", None))
        self.label.setText(_translate("mainWindow", "Please enter your Graph information", None))
        self.label_3.setText(_translate("mainWindow", "Result will be stored in:", None))
        self.lbldestText.setText(_translate("mainWindow", "Your selected input is: ", None))
        self.btnNavigate.setText(_translate("mainWindow", "Browse", None))
        self.comboBoxSteps.setItemText(0, _translate("mainWindow", "1 Step", None))
        self.comboBoxSteps.setItemText(1, _translate("mainWindow", "5 Steps", None))
        self.comboBoxSteps.setItemText(2, _translate("mainWindow", "10 Steps", None))
        self.comboBoxSteps.setItemText(3, _translate("mainWindow", "20 Steps", None))
        self.comboBoxSteps.setItemText(4, _translate("mainWindow", "100 Steps", None))
        self.btnShowResult.setText(_translate("mainWindow", "Show Result", None))
        self.btnKruskal.setText(_translate("mainWindow", "Execute", None))
        self.comboBoxAlgorithm1.setItemText(0, _translate("mainWindow", "Kruskal", None))
        self.comboBoxAlgorithm1.setItemText(1, _translate("mainWindow", "Prim", None))
        self.btnRefresh.setText(_translate("mainWindow", "Refresh", None))
        self.comboBoxAlgorithm.setItemText(0, _translate("mainWindow", "Kruskal", None))
        self.comboBoxAlgorithm.setItemText(1, _translate("mainWindow", "Prim", None))
        self.btnOneTimeExecution.setText(_translate("mainWindow", "Execute", None))
        self.label_5.setText(_translate("mainWindow", "Controlled Execution", None))
        self.browser.setText(_translate("mainWindow", "Browse", None))
        self.btnHelp.setText(_translate("mainWindow", "Help", None))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    mainWindow = QtGui.QMainWindow()
    ui = Ui_mainWindow()
    ui.setupUi(mainWindow)
    mainWindow.show()
    sys.exit(app.exec_())

