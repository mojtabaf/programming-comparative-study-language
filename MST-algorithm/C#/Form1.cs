﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Assignment2
{
    public partial class Form1 : Form
    {
        private bool beginning = true;
        private KruskalMST stepRunner;
        private List<Edge> mst = new List<Edge>();

        public Form1()
        {
            InitializeComponent();
        }

        private void selectGraphToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult result = openFileDialog1.ShowDialog();
                if (result == DialogResult.OK)
                {
                    this.lblSource.Text = openFileDialog1.FileName;
                }
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message, "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void selectResultPathToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            try
            {
                var result = saveFileDialog1.ShowDialog();
                if (result == DialogResult.OK)
                {
                    this.lblResult.Text = saveFileDialog1.FileName;
                }
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message, "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void showGraphInfoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                this.textBox1.Text = "";
                using (var reader = new StreamReader(this.lblSource.Text))
                {
                    string line;
                    while ((line = reader.ReadLine()) != null)
                    {
                        this.textBox1.AppendText(line + "\n");
                    }
                }
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message, "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void showResultToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                this.textBox1.Text = "";
                using (var reader = new StreamReader(this.lblResult.Text))
                {
                    string line;
                    while ((line = reader.ReadLine()) != null)
                    {
                        this.textBox1.AppendText(line + "\n");
                    }
                }
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message, "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void kruskalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                this.panel_steps.Visible = false;
                lblStatus.Text = "Status: Running!";
                var mst = new KruskalMST(this.lblSource.Text, this.lblResult.Text);
                if (mst.finder())
                {
                    this.textBox1.Text = "";
                    lblStatus.Text = "Status: Completed!";
                    MessageBox.Show("Minimum Spanning tree is calculated!", "Result", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message, "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            this.textBox1.SelectionStart = textBox1.Text.Length;
            this.textBox1.ScrollToCaret();
        }

        private void btnStepsExec_Click(object sender, EventArgs e)
        {
            if(this.lblSource.Text == "" || this.lblResult.Text == "")
            {
                this.textBox1.Text = "";
                this.lblStatus.Text = "Status: Not Started!";
                MessageBox.Show("Please select source and result path!", "Message", MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
            }
            else
            {
                try
                {
                    if(this.comboBox1.SelectedIndex < 0 || this.comboBox_steps.SelectedIndex < 0)
                    {
                        MessageBox.Show("Please select algorithm and running steps!", "Message", MessageBoxButtons.OK,
                            MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                        return;
                    }
                    if (this.beginning)
                    {
                        this.mst = new List<Edge>();
                        this.lblStatus.Text = "Status: Running";
                        this.textBox1.Text = "";
                        this.stepRunner = new KruskalMST(this.lblSource.Text, this.lblResult.Text);
                        var vertices = this.stepRunner.getVertices();
                        this.stepRunner.setQuickUnion(new WeightedQuickUnion(vertices));
                        this.textBox1.AppendText("Total number of nodes = " + vertices + "\n");
                        this.textBox1.AppendText("Total number of edges in the minimum spanning three = " + (vertices - 1) + "\n");
                        this.textBox1.AppendText("List of edges & their costs:\n");
                        this.beginning = false;
                    }
                    var index = 1;
                    if(this.stepRunner.edges.Count >= 1)
                    {
                        var selectedItem = this.comboBox_steps.SelectedIndex;
                        switch (selectedItem)
                        {
                            case 0:
                                index = 1;
                                break;
                            case 1:
                                index = 5;
                                break;
                            case 2:
                                index = 10;
                                break;
                            case 3:
                                index = 50;
                                break;
                            case 4:
                                index = 100;
                                break;
                            default:
                                break;
                        }
                        for (int i = 0; i < index; i++)
                        {
                            var edge = this.stepRunner.ControlledKruskal();
                            if(edge != null)
                            {
                                this.textBox1.AppendText(edge.toString() + "\n");
                                this.mst.Add(edge);
                            }
                            else
                            {
                                if(this.stepRunner.edges.Count >= 1)
                                {
                                    var node = this.stepRunner.edges[0];
                                    this.textBox1.AppendText(node.toString() + "\t\t******* Cycle happend *******\n");
                                    this.stepRunner.edges.Remove(node);
                                }

                            }
                            if (this.stepRunner.edges.Count < 1)
                            {
                                this.textBox1.AppendText("Total cost of minimum spanning three is = Sum of ");
                                this.textBox1.AppendText(this.stepRunner.getMSTWeight(this.mst));
                                this.btnStepsExec.Enabled = false;
                                this.lblStatus.Text = "Status: Completed!";
                                this.stepRunner = null;
                                break;
                            }
                        }            
                    }

                }
                catch (Exception exp)
                {
                    MessageBox.Show(exp.Message, "Message", MessageBoxButtons.OK,
                        MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                }
            }
        }

        private void stepsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.panel_steps.Visible = true;
            this.textBox1.Text = "";

        }

        private void resetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.comboBox1.SelectedIndex = 0;
            this.comboBox_steps.SelectedIndex = 0;
            this.textBox1.Text = "";
            this.beginning = true;
            this.btnStepsExec.Enabled = true;
            this.mst = new List<Edge>();
            this.stepRunner = null;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.SelectedItem.Equals("Prim"))
            {
                MessageBox.Show("The Prim algorithm is not availble right now!", "Message", MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                comboBox1.SelectedIndex = 0;
            }
        }

        private void helpToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Process process = new Process();
            ProcessStartInfo startInfo = new ProcessStartInfo();
            process.StartInfo = startInfo;
            startInfo.FileName = @"..\..\Help.pdf";
            process.Start();
        }
    }
}
