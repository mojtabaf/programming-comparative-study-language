﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment2
{
    class Edge : IComparable<Edge>
    {
        private int v;
        private int w;
        private int weight;

        public Edge(int v, int w, int weight)
        {
            this.v = v;
            this.w = w;
            this.weight = weight;
        }

        public Int32 getWeight()
        {
            return this.weight;
        }
        public int either()
        {
            return v;
        }

        public int other(int vertex)
        {
            if (vertex == v)
                return w;
            else
                return v;
        }

        public int CompareTo(Edge that)
        {
            return (this.weight).CompareTo(that.getWeight());
        }

        public string toString()
        {
            return "(" + v + ", " + w + ")" + "\tedge cost: " + weight;
        }

    }
}