import threading
from random import randint
import time


class SharedPlaceController:

    def __init__(self):
        self.reader_count = 0
        self.sharedList = []
        self.BOUND = 12
        self.lock = threading.Lock()
        self.condition = threading.Condition()
        self.lck = threading.Lock()
        self.counter = 0

    def read(self):
        with self.lock:
            self.reader_count += 1
            thread_name = threading.currentThread().getName()
            print("Thread " + thread_name + " now is ready to read  the shared location\t ")

        delay_base = 3000
        list_str = str(self.sharedList)
        print(list_str)
        delay = randint(1, delay_base)
        time.sleep(delay / 1000.0)
        thread_name = threading.currentThread().getName()
        print("Thread " + thread_name + " now has finished reading the shared location.")
        with self.lck:
            self.reader_count -= 1
        with self.condition:
            if self.reader_count == 0:
                    self.condition.notifyAll()

    def write(self):
        with self.lock:
            while self.reader_count != 0:
                with self.condition:
                    self.condition.wait()

            thread_name = threading.currentThread().getName()
            print("Thread " + thread_name + " now is ready to write the shared location.\t ")
            if len(self.sharedList) > self.BOUND:
                self.sharedList.pop(0)
            number = randint(1, 100)
            self.sharedList.append(number)
            delay_base = 3000
            delay = randint(1, delay_base)
            time.sleep(delay / 1000.0)
            print("Thread " + thread_name + "  now has finished writing the shared location. " + str(number))
            with self.condition:
                self.condition.notifyAll()

    def rworker(self):
        delay_base = 3000
        delay = randint(1, delay_base)
        time.sleep(delay / 1000.0)
        self.read()

    def wworker(self):
        delay_base = 3000
        delay = randint(1, delay_base)
        time.sleep(delay / 1000.0)
        self.write()

pass
