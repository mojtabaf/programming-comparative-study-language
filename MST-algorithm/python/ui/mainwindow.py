# -*- coding: utf-8 -*-

"""
Module implementing MainWindow.
"""

from PyQt4.QtCore import pyqtSignature,  QString,  QUrl,  QDir
from PyQt4.QtGui import QMainWindow,  QFileDialog,  QMessageBox,  QDesktopServices

from Ui_mainwindow import Ui_mainWindow
from core.kruskalMST import MSTFinder
import sys
from core.unionConectivity import WeightedQuickUnion


class MainWindow(QMainWindow, Ui_mainWindow):
    """
    Class documentation goes here.
    """
    def __init__(self, parent=None):
        """
        Constructor
        
        @param parent reference to the parent widget (QWidget)
        """
        QMainWindow.__init__(self, parent)
        self.setupUi(self)
        self.firsttry = True
        self.summation = []
        self.btnShowResult.hide()
        self.k = None
    
    @pyqtSignature("")
    def on_browser_released(self):
        """
        Slot documentation goes here.
        """
        fName = QFileDialog.getOpenFileName(None, self.trUtf8("Select your graph information"), QString(), self.trUtf8("*.txt"), None)
        self.lblPath.setText(fName)
    
    @pyqtSignature("")
    def on_btnHelp_released(self):
        """
        Slot documentation goes here.
        """
        QDesktopServices.openUrl(QUrl.fromLocalFile(QDir.currentPath()+"/Help.pdf"));
        #QDesktopServices.openUrl(QUrl(self.currentDir.absoluteFilePath("/home/mojtaba/Documents/CMST/build-Mst-Desktop_Qt_5_7_0_GCC_64bit-Debug/Help.pdf")))
    
    @pyqtSignature("")
    def on_btnNavigate_released(self):
        """
        Slot documentation goes here.
        """
        fName = QFileDialog.getSaveFileName(None, self.trUtf8("Select the path where you want to save the mst result"), QString(), self.trUtf8("*.txt"), None)
        self.lblResult.setText(fName)
    
    @pyqtSignature("")
    def on_btnShowResult_released(self):
        """
        Slot documentation goes here.
        """
        with open(self.lblResult.text(), 'r') as f:
            lines = f.readlines()
            for line in lines:
                self.textBrowser.append(line + "\n")
    
    @pyqtSignature("")
    def on_btnKruskal_clicked(self):
        """
        Slot documentation goes here.
        """
        if self.comboBoxAlgorithm1.currentText() == "Prim":
            self.lblStatus.setText("Current Status: Not Started!")
            self.textBrowser.setText("Prim algorithm has not been implemented yet!")
            self.btnShowResult.hide()
        else:
            self.textBrowser.setText("")
            self.lblStatus.setText("Current Status: In Progress!")
            if self.lblPath.text() == "" or self.lblResult.text() == "":
                self.textBrowser.setText("")
                self.lblStatus.setText("Current Status: Not Started!")
                QMessageBox.critical(self, 'Message!', "please select your graph information and result path", QMessageBox.Ok)
            else:
                try:
                    k = MSTFinder(self.lblPath.text(), self.lblResult.text())
                    if k.finder():
                        self.lblStatus.setText("Current Status: Done!")
                        self.btnShowResult.show()
                except:
                    e = sys.exc_info()[0]
                    QMessageBox.critical(self, 'Message!', str(e), QMessageBox.Ok)
    
    @pyqtSignature("")
    def on_btnRefresh_released(self):
        """
        Slot documentation goes here.
        """
        # TODO: not implemented yet
        self.summation = []
        self.firsttry = True
        self.btnOneTimeExecution.setEnabled(True)
        self.textBrowser.setText("")
        self.lblStatus.setText("Current Status: Not Started!")
        self.k = None
    
    @pyqtSignature("")
    def on_btnOneTimeExecution_pressed(self):
        """
        Slot documentation goes here.
        """
        if self.lblPath.text() == "" or self.lblResult.text() == "":
                self.textBrowser.setText("")
                self.lblStatus.setText("Current Status: Not Started!")
                QMessageBox.critical(self, 'Message!', "please select your graph information and result path", QMessageBox.Ok)
        else:
            if self.comboBoxAlgorithm.currentText() == "Prim":
                self.lblStatus.setText("Current Status: Not Started!")
                QMessageBox.information(self, 'Message!', "Prim algorithm has not been implemented yet!", QMessageBox.Ok)
            else:
                try:
                    if self.firsttry:
                        self.summation = []
                        self.lblStatus.setText("Current Status: In Progress!")
                        self.textBrowser.setText("")
                        self.k = MSTFinder(self.lblPath.text(), self.lblResult.text())
                        info = self.k.reader()
                        vertices = info[0]
                        unioninfo = WeightedQuickUnion(vertices)
                        self.k.setStepedQuickUnion(unioninfo)
                        self.textBrowser.append("Total number of nodes = " + str(vertices) + "\n")
                        self.textBrowser.append("Total number of edges in the minimum spanning three = " + str(vertices -1) + "\n")
                        self.textBrowser.append("List of edges & their costs:\n")
                        self.firsttry = False
                    steps = 1
                    if len(self.k.edges) >= 1:
                        selected = self.comboBoxSteps.currentIndex()
                        if selected == 0:
                            steps = 1
                        elif selected == 1:
                            steps = 5
                        elif selected == 2:
                            steps = 10
                        elif selected == 3:
                            steps = 20
                        elif selected == 4:
                            steps = 100
                        for i in range(steps):
                            edge = self.k.controlledKruskal()
                            if edge is not None:
                                self.textBrowser.append("(" + str(edge[0]) + ", " + str(edge[1]) + ")\tedge cost: " + str(edge[2]) + "\n")
                                self.summation.append(edge)
                            else:
                                if len(self.k.edges) >= 1:
                                    node  = self.k.edges[0]
                                    self.textBrowser.append("(" + str(node[0]) + ", " + str(node[1]) + ")\tedge cost: " + str(node[2]) + " --> Cycle \n")
                                    self.k.edges.remove(self.k.edges[0])
                            if len(self.k.edges) < 1:
                                self.textBrowser.append("Total cost of minimum spanning three is = Sum of ")
                                self.textBrowser.append(self.k.getMSTWeight(self.summation))
                                self.btnOneTimeExecution.setEnabled(False)
                                self.lblStatus.setText("Current Status: Done!")
                                self.k = None
                                break
                except:
                    e = sys.exc_info()[0]
                    QMessageBox.critical(self, 'Message!', str(e), QMessageBox.Ok)
                

